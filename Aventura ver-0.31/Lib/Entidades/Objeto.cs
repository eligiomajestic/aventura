﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Lib.Entidades
{
    public enum enumColision {ARRIBA,ABAJO,DERECHA,IZQUIERDA,TODAS}

    public class Objeto : Vaina
    {
        public Vector2 posicion;
        public Texture2D spriteTextura;
        public Rectangle rectangulo;
        public Vector2 tamanio;
        public float profundidad;
        public bool boolColisionable = true;
        public Vector2 velocidadObjeto;
        public Color Colorante = Color.White;

        public Objeto() { }
        public Objeto(Texture2D textura,Vector2 posicion) 
        {
            spriteTextura = textura;
            this.posicion = posicion;

            profundidad = 0.5f;

            rectangulo = new Rectangle((int)posicion.X,(int)posicion.Y,spriteTextura.Width,spriteTextura.Height);

            if(textura == Juego.texturaNube)
            {
                velocidadObjeto.X = 1.5f - (posicion.Y / 150f);
            }

        }

        public void Pintate(SpriteBatch batch)
        {           
            batch.Draw(spriteTextura, rectangulo,null, Color.White,0f,Vector2.Zero,SpriteEffects.None,profundidad);
        }
        public void Actualizar(GameTime dataTime)
        {
            rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, spriteTextura.Width, spriteTextura.Height);
            // AI para mover nubes en el eje X
            if (spriteTextura == Juego.texturaNube) 
            {
                AINubes();
            }
        }

        //Ai para mover las nubes
        public void AINubes()
        {
            if (posicion.X > 8000)
            {
                posicion.X = -500;
            }

            posicion.X += velocidadObjeto.X;
        }

        public bool Colision(List<Objeto> listaDeObjetos, enumColision direccion) 
        {
            bool resultado = false;

            foreach(Objeto i in listaDeObjetos)
            {
                if (i.boolColisionable)
                {
                    switch (direccion)
                    {
                        #region Todas
                        case enumColision.TODAS:

                            if (rectangulo.Intersects(i.rectangulo))
                            {
                                resultado = true;
                            }

                            break;
                        #endregion

                        #region Abajo
                        case enumColision.ABAJO:
                            if (new Rectangle(rectangulo.X + 15, rectangulo.Y + (int)tamanio.Y, rectangulo.Width - 30, 1).Intersects(i.rectangulo))
                            {
                                if (!new Rectangle(rectangulo.X + 15, rectangulo.Y + (int)tamanio.Y - 30, rectangulo.Width - 15, 2).Intersects(i.rectangulo))
                                {
                                    resultado = true;
                                    posicion.Y = i.rectangulo.Y - rectangulo.Height + 2;
                                } 
                            }

                            break;
                        #endregion

                        #region Arriba
                        case enumColision.ARRIBA:
                            if (new Rectangle(rectangulo.X + 15, rectangulo.Y , rectangulo.Width - 30, 2).Intersects(i.rectangulo))
                            {
                                if (!new Rectangle(rectangulo.X + 15, rectangulo.Y + 30, rectangulo.Width - 15, 1).Intersects(i.rectangulo))
                                {
                                    resultado = true;
                                    posicion.Y = i.rectangulo.Y + i.rectangulo.Height + 2;
                                }
                            }

                            break;
                        #endregion

                        #region Derecha
                        case enumColision.DERECHA:
                            if (new Rectangle(rectangulo.X + rectangulo.Width -5,rectangulo.Y +5, 5, rectangulo.Height -30).Intersects(i.rectangulo))
                            {
                                if (!new Rectangle(rectangulo.X + rectangulo.Width - 30, rectangulo.Y, 5, rectangulo.Height -30).Intersects(i.rectangulo))
                                {
                                    resultado = true;
                                    posicion.X = i.rectangulo.X - rectangulo.Width;
                                }
                            }
                            break;
                        #endregion

                        #region Izquierda
                        case enumColision.IZQUIERDA:

                            if (new Rectangle(rectangulo.X , rectangulo.Y + 5, 5, rectangulo.Height - 30).Intersects(i.rectangulo))
                            {
                                if (!new Rectangle(rectangulo.X + 30, rectangulo.Y, 5, rectangulo.Height - 30).Intersects(i.rectangulo))
                                {
                                    resultado = true;
                                    posicion.X = i.rectangulo.X + i.rectangulo.Width;
                                }
                            }
                            break;
                        #endregion
                    }
                }
            }
            return resultado;
        }
        public bool ColisionConmigo(Rectangle rect)
        {
            if (rectangulo.Intersects(rect))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
