﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public class Top
    {
        public Texture2D sprite;
        public Vector2 posicion;
        public Rectangle rectangulo;
        public float profundidad;
        public int numeroDePlayer;
        public float salud;
        public Color colorante;

        public Top(int NumeroDePlayer, Color color)
        {
            sprite = Juego.topKiritoTexture;
            profundidad = 0f;
            numeroDePlayer = NumeroDePlayer;
            colorante = color;
        }

        public void Pintate(SpriteBatch batch)
        {
            batch.Draw(sprite, rectangulo, null, colorante, 0f, Vector2.Zero, SpriteEffects.None, profundidad);

            //pintamos barra de abajo roja
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2) -5, (int)posicion.Y + (Juego.topKiritoTexture.Height/3), 100, 15), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.DarkRed, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.03f);
            //pintamos la cantidad de vida que tiene salud, color verde
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2)-5, (int)posicion.Y + (Juego.topKiritoTexture.Height/3), (int)(100 * ((double)salud / 100)), 15), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.ForestGreen, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.02f);
            //pintamos el marco
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2)-5, (int)posicion.Y + (Juego.topKiritoTexture.Height/3), 100, 15), new Rectangle(0, 0, Juego.HealthBar.Width, 44), Color.White, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.01f);
            //pintamos puntaje monedas y vidas
            PintarPuntajeDePersonajes(batch);
        }
        public void Actualizar(GameTime dataTime, float health)
        {
            posicion.X = -Juego.camara.Position.X + ((numeroDePlayer-1) * Juego.topKiritoTexture.Width) ; 



            rectangulo = new Rectangle((int)posicion.X,(int)posicion.Y,Juego.topKiritoTexture.Width,Juego.topKiritoTexture.Height);


            salud = health;
        }
        public void PintarPuntajeDePersonajes(SpriteBatch batch) 
        {
            if(numeroDePlayer == 1)
            {
                batch.DrawString(Juego.Texto, ("Monedas : " + Juego.takashi.monedas.ToString()), new Vector2(posicion.X,posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion : " + Juego.takashi.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width/3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 2)
            {
                batch.DrawString(Juego.Texto, ("Monedas : " + Juego.takashi2.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion : " + Juego.takashi2.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi2.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 3)
            {
                batch.DrawString(Juego.Texto, ("Monedas : " + Juego.takashi3.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion : " + Juego.takashi3.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi3.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 4)
            {
                batch.DrawString(Juego.Texto, ("Monedas : " + Juego.takashi4.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion : " + Juego.takashi4.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi4.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
        }
    }
}
