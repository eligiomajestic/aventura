﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public enum enumTipoPowerUp { VelocidadPlus , ActivacionPower, CURACION ,Moneda}
    public class PowerUp : Objeto
    {
        public enumTipoPowerUp tipoPower;
        public bool activado;
        public float bonoVelocidad;
        public int vida;
        public Animacion aniMoneda;

        public PowerUp(Vector2 posicion, enumTipoPowerUp tipo) 
        {
            tamanio = new Vector2(40,30);
            this.activado = true;
            bonoVelocidad = 0.1f;
            this.posicion = posicion;
            this.vida = 30;
            tipoPower = tipo;
            profundidad = 0.09f;
            if (tipoPower != enumTipoPowerUp.Moneda)
            {
                if(tipoPower == enumTipoPowerUp.CURACION)
                    rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, Juego.TexturaPowerUpVida.Width, Juego.TexturaPowerUpVida.Height);
            }
            else
            {
                rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, 50, 50);
            }

            aniMoneda = new Animacion(Juego.graphics, posicion);
            aniMoneda.adicionarImagen(Juego.TexturaMoneda, 12);
            aniMoneda.iniciar(0, 11, 0.4f, true);
        }
        //NUEVO - actualizar, en progreso...
        public void Actualizar(GameTime dataTime)
        {
            if (activado)
            {
                //Actualizamos la moneda
                if (tipoPower == enumTipoPowerUp.Moneda)
                {
                    aniMoneda.Actualizar(dataTime, true);
                }
            }

        }

        //NUEVO - pintate del power up
        public  void Pintate(SpriteBatch batch)
        {
            if (activado)
            {
                if (tipoPower == enumTipoPowerUp.Moneda) 
                {
                    aniMoneda.Dibujar(batch, profundidad, Color.White);
                }
                else
                batch.Draw(Juego.TexturaPowerUpVida, rectangulo, null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, profundidad);
            }
        }

    }
}
