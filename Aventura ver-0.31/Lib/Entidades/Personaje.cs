﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public enum Enumspersonaje {PLAYER, ENEMIGO, ADORNO,BOSS}
    public enum EnumPlayerX {CAMINANDOderecha,CAMINANDOizquierda,NORMAL, MUERTO}
    public enum EnumPlayerY { SALTANDO, NORMAL,}
    public enum EnumPlayerAtaques {NINGUNO,LANZANDO,ATAQUENORMAL}


    public class Personaje : Objeto
    {
        #region Variables
        public List<Ataque> listaDeAtaque;

        private GameTime dataTimePersonaje;
        public Animacion aniActual;

        public bool boolCambioArma;
        public bool boolLanzo;
        public bool boolpowerUp;
        public bool congelado;
        public bool inmortal = false;

        public Animacion aniStandar;
        public Animacion aniAtaqueNormal;
        public Animacion aniCaminando;
        public Animacion aniSalto;
        public Animacion aniMuertePersonaje;
        public Animacion aniAtaqueLanzador;
        public Animacion aniPowerUp;

        public EnumsAtaque ataqueSelet;
        public PlayerIndex index;
        public bool mirandoDeracha;
        // Bool para saber cuando un player o un enemigo esta muerto o vivo
        public bool AlivePersonaje;
        public bool Invensible;
        public Enumspersonaje enumTipo;
        public EnumPlayerX enumPlayerEstadoXUltimo;
        public EnumPlayerAtaques enumPlayerAtaques;
        public EnumPlayerX enumPlayerEstadoX;
        public EnumPlayerY enumPlayerEstadoY;

        //Vidas del jugador
        public int Vidas;
        //Salud del jugador y enemigo
        public int Health;

        public int CongelacionesPosibles = 3;
        public int monedas;
        public int puntuacion;
        public int numeroDePlayer;
        public int Tiempo;
        public int MaximaSaludPlayer;

        public float velocidadMaximaPorSegundo;
        public float escalaVelocidad;
        public float escalaSalto;
        public float friccion;
        public float tiempoCongelado;
        public int TamnioDeBarra;
        public Vector2 velocidad;
        public Vector2 aceleracion;
        public Vector2 posicionRetorno;

        //Ponemos un top
        public Top top;
        #endregion

        public Personaje(Enumspersonaje TIPO,Vector2 Posicion, int NumerePlayer = 1)
        {
            
            listaDeAtaque = new List<Ataque>();
            posicion = Posicion;
            aceleracion = Vector2.Zero;
            velocidad = Vector2.Zero;
            velocidadMaximaPorSegundo = 5;
            enumTipo = TIPO;
            monedas = 0;
            puntuacion = 0;
            
            escalaVelocidad = 50;
            escalaSalto = 250;
            friccion = 50f;
            mirandoDeracha = true;
            Tiempo = 0;
            AlivePersonaje = true;
            boolLanzo = true;
            enumPlayerAtaques = EnumPlayerAtaques.NINGUNO;
            boolpowerUp = false;
            Invensible = false;
            boolCambioArma = true;
            congelado = false;
            tiempoCongelado = 0;

            if (TIPO == Enumspersonaje.PLAYER)
            {
                numeroDePlayer = NumerePlayer;
                tamanio = new Vector2(45, 55);
                profundidad = 0.4f;
                enumPlayerEstadoX = EnumPlayerX.NORMAL;
                enumPlayerEstadoY = EnumPlayerY.NORMAL;
                Vidas = 3;
                Health = 100;
                MaximaSaludPlayer = 100;
                spriteTextura = Juego.topKiritoTexture;
                if (numeroDePlayer == 2) { Colorante = Color.Yellow; } else if (numeroDePlayer == 3) { Colorante = Color.LightGreen; } else if (numeroDePlayer == 4) { Colorante = Color.LightCoral; }
                top = new Top(numeroDePlayer,Colorante);
                ataqueSelet = EnumsAtaque.Fuego;
            }
            else if (TIPO == Enumspersonaje.ENEMIGO) 
            {
                profundidad = 0.4f;
                tamanio = new Vector2(45, 55);
                enumPlayerEstadoX = EnumPlayerX.NORMAL;
                Vidas = 1;
                Health = 20 * ((int)Juego.Dificultad +1);
                MaximaSaludPlayer = 20 * ((int)Juego.Dificultad + 1);
                TamnioDeBarra = 75;
            }
            else if (TIPO == Enumspersonaje.BOSS)
            {
                profundidad = 0.1f;
                tamanio = new Vector2(125, 150);
                enumPlayerEstadoX = EnumPlayerX.NORMAL;
                Vidas = 1;
                Health = 1000 * ((int)Juego.Dificultad + 1);
                MaximaSaludPlayer = 1000 * ((int)Juego.Dificultad + 1);
                TamnioDeBarra = 150;
                
            }
        }

        public void Actualizar(GameTime dataTime)
        {
            
            this.dataTimePersonaje = dataTime;
            #region Global O que se le aplican a todos los personajes

            // controlamos la inmortalidad
            inmortal = Inmortalidad();

            //Actualizamos la barra de salud
            ActualizarBarraDeSaludPlayer();

            //Mata el personaje si sale de pantalla hacia abajo
            MuertePorCaidaFueraDePantalla();

            //Si esta vivo, mira su HP y dime si puedo matarlo >:D
            if (AlivePersonaje)
            {
                ActualizarVivoOMuerto();
            }
            else
                congelado = false;

            //actualizamos Ataques
            foreach (Ataque i in listaDeAtaque)
            {
                i.Actualizar(dataTime);
                //aqui damos color a los ataques igual que el del personaje
                if (i.Colorante != Colorante)
                { i.Colorante = Colorante; }
            }

            //Desconjelar luego de un tiempo
            if (congelado)
            {
                Descongelar(dataTime);
            }
            else
            {
                tiempoCongelado = 0;
            }

            if (!congelado)
            {
                //Aplicammos la fisica
                Fisica(Juego.MapaActual.listaColisionMapa, dataTime);

                //actualizamos la velocidad
                velocidad += aceleracion * (float)dataTime.ElapsedGameTime.TotalSeconds;

                //limitador de velocidad
                LimitesDeVelocidad();

                //Cambiamos posicion
                posicion += velocidad;

                //actualizamos rectangulo
                rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, (int)tamanio.X, (int)tamanio.Y);

                //Llamamos al Actualizar de las animacines cambios
                AnimacionActualizar(dataTime);

                //Actualizamos la animacion
                aniActual.Actualizar(dataTime, mirandoDeracha);
                
            }

            #endregion
            switch (enumTipo)
            {
                case Enumspersonaje.PLAYER:
                    #region Personaje

                    // linitamos posicion al personaje dependiendo de la camara
                    LimitesDePosicion();                              

                    //Si el jugador intersecta un oponente, pierde un punto de vida - prueba
                    ColisionPersonajeConListaDePersonaje(Juego.MapaActual.listaEnemigos);

                    //Verificamos colision con power ups
                    ColisionaPowerUp();

                    //Verificamos si recibe daño
                    VerificacionDanioConAtaques(Juego.MapaActual.listaEnemigos);

                    //actualizamos el top
                    top.Actualizar(dataTime,Health);

                    // verifica si el personaje puede revivir
                    RevivirPersonaje();

                    

                    break;
                    #endregion

                case Enumspersonaje.ENEMIGO:
                    #region Enemigo


                    //Verificamos si recibe daño
                    VerificacionDanioConAtaques(Juego.listaPersonajesVivos);

                    if (AlivePersonaje)
                    {
                        IA();
                    }
                    else
                    {
                        rectangulo = Rectangle.Empty;
                    }


                    break;
                    #endregion

                case Enumspersonaje.ADORNO:
                    #region Adorno
                    break;
                    #endregion

                case Enumspersonaje.BOSS:
                    #region Boss

                    //Verificamos si recibe daño
                    VerificacionDanioConAtaques(Juego.listaPersonajesVivos);

                    //Inteligencia
                    IABoss();

                    aniActual.ponerPosicion(posicion.X - 30, posicion.Y + 10);
                    break;
                    #endregion
            }

            
        }
        public void Pintate(SpriteBatch batch)
        {
            aniActual.Dibujar(batch, profundidad,Colorante);

            //actualizamos Ataques
            foreach (Ataque i in listaDeAtaque)
            {
                i.Pintate(batch);
            }

            if (enumTipo == Enumspersonaje.ENEMIGO || enumTipo == Enumspersonaje.BOSS)
            {
                //pintamos barra de salud
                PintarBarraDeSalud(batch);
            }

            if (enumTipo == Enumspersonaje.PLAYER)
            {
                //Pintamos el Top
                top.Pintate(batch,ataqueSelet);
                if (inmortal) 
                {
                    batch.Draw(Juego.AtaqueEscudo, new Rectangle((int)posicion.X - 23,(int)posicion.Y - 20,Juego.AtaqueEscudo.Width,Juego.AtaqueEscudo.Height), Colorante);
                }
            }

            if (enumTipo == Enumspersonaje.BOSS)
            {
                //Pintamos el Top
                if (inmortal)
                {
                    batch.Draw(Juego.AtaqueEscudo, new Rectangle((int)posicion.X - 23, (int)posicion.Y - 20, Juego.AtaqueEscudo.Width, Juego.AtaqueEscudo.Height), Colorante);
                }
            }
        }

        public void InicializarAnimaciones() 
        {
            aniAtaqueNormal = new Animacion(Juego.graphics, posicion);
            aniActual = new Animacion(Juego.graphics, posicion);
            aniStandar = new Animacion(Juego.graphics, posicion);
            aniCaminando = new Animacion(Juego.graphics, posicion);
            aniSalto = new Animacion(Juego.graphics, posicion);
            aniMuertePersonaje = new Animacion(Juego.graphics, posicion);
            aniAtaqueLanzador = new Animacion(Juego.graphics, posicion);
            aniPowerUp = new Animacion(Juego.graphics, new Vector2(posicion.X, posicion.Y-5555));
        }
        public void AnimacionActualizar(GameTime dataTime)
        {
            //Aqui comprobamos si el jugador esta con vida para ejecutar las siguientes animaciones
            if (AlivePersonaje)
            {
                if (!boolpowerUp)
                {
                    if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
                    {
                        if (enumPlayerAtaques == EnumPlayerAtaques.NINGUNO)
                        {//aqui esta en el piso y no esta atacando
                            switch (enumPlayerEstadoX)
                            {
                                case EnumPlayerX.NORMAL:
                                    aniActual = aniStandar;
                                    break;
                                case EnumPlayerX.CAMINANDOderecha:
                                    aniActual = aniCaminando;
                                    break;
                                case EnumPlayerX.CAMINANDOizquierda:
                                    aniActual = aniCaminando;
                                    break;
                            }
                        }
                        else
                        {
                            //aqui esta en el piso y esta atacando
                            switch (enumPlayerAtaques)
                            {
                                case EnumPlayerAtaques.LANZANDO:
                                    aniActual = aniAtaqueLanzador;
                                    break;
                                case EnumPlayerAtaques.ATAQUENORMAL:
                                    aniActual = aniAtaqueNormal;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (enumPlayerAtaques == EnumPlayerAtaques.NINGUNO)
                        {
                            
                            aniActual = aniSalto;
                        }
                        else
                        {
                            //aqui esta en el Aire y esta atacando ojo solo ataques que puedan tirar en el aire
                            switch (enumPlayerAtaques)
                            {
                                case EnumPlayerAtaques.LANZANDO:
                                    aniActual = aniAtaqueLanzador;
                                    break;
                                case EnumPlayerAtaques.ATAQUENORMAL:
                                    aniActual = aniAtaqueNormal;
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    //prueba
                    //subiendole unos pixeles a la animacion de power up cuando esta en el piso solo
                    if (aniActual != aniSalto)
                    {
                        posicion.Y -= 5;
                        aniActual = aniPowerUp;
                    }
                    if (enumPlayerEstadoY == EnumPlayerY.SALTANDO)
                    {
                        posicion.Y += 5;
                        aniActual = aniPowerUp;
                    } 
                }

            }
              //Si entra aqui quiere decir que el booleano era falso, por lo que el jugador esta muerto :( 
            //entonces entra la animacion de muerte del personaje, que se ejecuta una sola vez
            else
            {
                aniActual = aniMuertePersonaje;
            }

            aniActual.Actualizar(dataTime, mirandoDeracha);
            reiniciarAnimacionesNoRepetible();
            aniActual.ponerPosicion(posicion.X, posicion.Y);
            ControlDeAnimaciones();

        }
        public void CargarAnimaciones() 
        {
            switch (enumTipo)
            {
                case Enumspersonaje.PLAYER:
                    // animacion standar
                    aniStandar.adicionarImagen(Juego.PlayerTexturaStandar, 4);
                    aniStandar.iniciar(0, 3, 1f, true);

                    //animacion caminando
                    aniCaminando.adicionarImagen(Juego.PlayerTexturaCaminando, 3);
                    aniCaminando.iniciar(0, 2, 0.7f, true);

                    //animacion Saltando
                    aniSalto.adicionarImagen(Juego.PlayerTexturaSaltando, 2);
                    aniSalto.iniciar(0, 1, 1f, true);

                    //animacion tomando power up
                    aniPowerUp.adicionarImagen(Juego.PlayerTexturaPowerUp, 6);
                    aniPowerUp.iniciar(0, 5, 1.2f, false);

                    //animacion muerte
                    aniMuertePersonaje.adicionarImagen(Juego.PlayerTexturaMuerte, 4);
                    aniMuertePersonaje.iniciar(0, 3, 1.2f, false);


                    //animacion Lanzador
                    aniAtaqueLanzador.adicionarImagen(Juego.AtaqueLanzador, 8);
                    aniAtaqueLanzador.iniciar(0, 7, 0.5f, false);

                    // animacion Atque standar
                    aniAtaqueNormal.adicionarImagen(Juego.PlayerTexturaAtaqueNormal, 4);
                    aniAtaqueNormal.iniciar(0, 3, 0.7f, false);

                    break;

                case Enumspersonaje.ENEMIGO:

                        // animacion standar
                        aniStandar.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniStandar.iniciar(0, 4, 1f, true);

                        //animacion caminando
                        aniCaminando.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniCaminando.iniciar(0, 4, 1f, true);

                        //animacion salto
                        aniSalto.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniSalto.iniciar(0, 4, 1f, true);

                        //animacion muerte
                        aniMuertePersonaje.adicionarImagen(Juego.EnemigoMuerteTextura, 8);
                        aniMuertePersonaje.iniciar(0, 7, 0.5f, false);

                    break;

                case Enumspersonaje.ADORNO:
                    break;

                case Enumspersonaje.BOSS:
                    // animacion standar
                    aniStandar.adicionarImagen(Juego.bossTexturaStandar, 4);
                    aniStandar.iniciar(0, 3, 0.8f, true);

                    //animacion caminando
                    aniCaminando.adicionarImagen(Juego.bossTexturaStandar, 4);
                    aniCaminando.iniciar(0, 3, 0.8f, true);

                    //animacion Salto
                    aniSalto.adicionarImagen(Juego.bossTexturaStandar, 4);
                    aniSalto.iniciar(0, 3, 0.8f, true);

                    //animacion muerte
                    aniMuertePersonaje.adicionarImagen(Juego.EnemigoMuerteTextura, 8);
                    aniMuertePersonaje.iniciar(0, 7, 0.5f, false);
                    break;
            }
        }
        public void reiniciarAnimacionesNoRepetible()
        {
            if (boolpowerUp) 
            {
                if (aniActual.frames == 5)
                {
                    boolpowerUp = false;
                    aniActual.frames = 0;
                }
            }
            else
            {
                if (enumPlayerAtaques == EnumPlayerAtaques.LANZANDO)
                {
                    if (aniActual.frames == 7)
                    {
                        enumPlayerAtaques = EnumPlayerAtaques.NINGUNO;
                        aniActual.frames = 0;
                        boolLanzo = true;
                        GamePad.SetVibration(index, 0f, 0f);
                    }
                }
                if (enumPlayerAtaques == EnumPlayerAtaques.ATAQUENORMAL) 
                {
                    if (aniActual.frames >= 3) 
                    {
                        enumPlayerAtaques = EnumPlayerAtaques.NINGUNO;
                        aniActual.frames = 0;
                        GamePad.SetVibration(index, 0f, 0f);
                    }
                }
            }
        }
        public void ControlDeAnimaciones() 
        {
            if (aniActual == aniAtaqueNormal) 
            {
                if (mirandoDeracha) 
                {
                    aniActual.ponerPosicion(posicion.X - 10,posicion.Y - 40);
                }
                else 
                {
                    aniActual.ponerPosicion(posicion.X - 60, posicion.Y - 40);
                }
            }
        }

        public void Saltar()
        {
            if (!congelado)
            {
                aceleracion.Y -= escalaSalto * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
                velocidad.Y += aceleracion.Y;
                aceleracion.Y = 0;
                enumPlayerEstadoY = EnumPlayerY.SALTANDO;
            }
            
        }
        public void MoverIzquierda()
        {
            if (!inmortal)
            {
                if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
                {
                    aceleracion.X -= escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
                    enumPlayerEstadoX = EnumPlayerX.CAMINANDOizquierda;
                }
                else
                {
                    aceleracion.X -= (escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds) / 2;
                    enumPlayerEstadoX = EnumPlayerX.CAMINANDOizquierda;
                }
                //aqui volteamos al jugador
                mirandoDeracha = false;
            }
        }
        public void MoverDerecha()
        {
            if (!inmortal)
            {
                if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
                {
                    aceleracion.X += escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
                    enumPlayerEstadoX = EnumPlayerX.CAMINANDOderecha;
                }
                else
                {
                    aceleracion.X += (escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds) / 2;
                    enumPlayerEstadoX = EnumPlayerX.CAMINANDOderecha;
                }
                //aqui volteamos al jugador
                mirandoDeracha = true;
            }
        }

        //Funcion para matar personajes :S
        public void ActualizarVivoOMuerto()
        {
            if(Health <= 0)
            {
                AlivePersonaje = false;
                congelado = false;
                tiempoCongelado = 5;
            }
        }
        public void ActualizarBarraDeSaludPlayer()
        {
            Health = (int)MathHelper.Clamp(Health, 0, MaximaSaludPlayer);
        }
        public void PintarBarraDeSalud(SpriteBatch batch) 
        {
            if (AlivePersonaje)
            {
                //pintamos barra de abajo griss
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10, TamnioDeBarra, 10), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.DarkRed, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.03f);
                //pintamos la cantidad de vida que tiene salud
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10,(int)(((float)Health/(float)MaximaSaludPlayer) * (float)TamnioDeBarra), 10), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.ForestGreen, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.02f);
                //pintamos el marco
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10, TamnioDeBarra, 10), new Rectangle(0, 0, Juego.HealthBar.Width, 44), Color.White, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.01f);
            }
        }

        //Funcion que me permite ver si se sale de la pantalla hacia abajo, si tiene vidas restale 1 y colocalo en posicion inicial
        public void MuertePorCaidaFueraDePantalla()
        {
            if (posicion.Y > Juego.fondo1.Height)
            {                
                Health = 0;
                if (enumTipo != Enumspersonaje.ENEMIGO)
                {
                    if (velocidad.X >= 0)
                        posicionRetorno.X -= 30;
                    else
                        posicionRetorno.X += 30;

                    posicionRetorno.Y -= 32;
                    velocidad = Vector2.Zero;
                    aceleracion = Vector2.Zero;
                    posicion = posicionRetorno;
                }
            }
        }
        public void RevivirPersonaje()
        {
            if (Health <= 0 && Vidas > 0)
            {
                Vidas -= 1;
                Health = MaximaSaludPlayer;
                AlivePersonaje = true;
            }
        }

        public void LimitesDeVelocidad()
        {
            if (enumPlayerEstadoXUltimo != enumPlayerEstadoX && enumPlayerEstadoX != EnumPlayerX.NORMAL) 
            {
                aceleracion.X = 0;
            }

            if(velocidad.X < -velocidadMaximaPorSegundo && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
            {
                velocidad.X = -velocidadMaximaPorSegundo;
            }

            if (velocidad.X > velocidadMaximaPorSegundo && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
            {
                velocidad.X = velocidadMaximaPorSegundo;
            }

            if (velocidad.Y > 20)
            {
                velocidad.Y = 20;
            }

            enumPlayerEstadoXUltimo = enumPlayerEstadoX;

            //prueba
            if (Math.Abs(velocidad.X) > velocidadMaximaPorSegundo) 
            {

            }
        }
        public void LimitesDePosicion() 
        {
            if (posicion.X > -(Juego.camara.Position.X) + Juego.graphics.GraphicsDevice.Viewport.Width - 100) 
            {
                if (velocidad.X > 0)
                    velocidad.X = 0;
            }
            else if (posicion.X < -(Juego.camara.Position.X) )
            {
                if (velocidad.X < 0)
                    velocidad.X = 0;
            }

        }
        public void AnularAceleracion(KeyboardState teclado) 
        {
            switch(numeroDePlayer)
            {
                case 1:
                    if ((teclado.IsKeyUp(Keys.Left) && Juego.control1.DPad.Left == ButtonState.Released && Juego.control1.ThumbSticks.Left.X > -0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                    if ((teclado.IsKeyUp(Keys.Right) && Juego.control1.DPad.Right == ButtonState.Released && Juego.control1.ThumbSticks.Left.X < 0.1) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 2:
                if ((teclado.IsKeyUp(Keys.A) && Juego.control2.DPad.Left == ButtonState.Released && Juego.control2.ThumbSticks.Left.X > -0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                if ((teclado.IsKeyUp(Keys.D) && Juego.control2.DPad.Right == ButtonState.Released && Juego.control2.ThumbSticks.Left.X < 0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 3:
                    if ((teclado.IsKeyUp(Keys.F) && Juego.control3.DPad.Left == ButtonState.Released && Juego.control3.ThumbSticks.Left.X > -0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                    if ((teclado.IsKeyUp(Keys.H) && Juego.control3.DPad.Right == ButtonState.Released && Juego.control3.ThumbSticks.Left.X < 0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 4:
                    if ((teclado.IsKeyUp(Keys.Delete) && Juego.control4.DPad.Left == ButtonState.Released && Juego.control4.ThumbSticks.Left.X > -0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                    if ((teclado.IsKeyUp(Keys.PageDown) && Juego.control4.DPad.Right == ButtonState.Released && Juego.control4.ThumbSticks.Left.X < 0.1f) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;
            }
        }

        public void Atacar(EnumsAtaque tipo)
        {
            listaDeAtaque.Add(new Ataque(posicion,tipo,mirandoDeracha,velocidad));

            if (tipo == EnumsAtaque.Fuego) 
            {
                enumPlayerAtaques = EnumPlayerAtaques.LANZANDO;
                boolLanzo = false;
            }
            else if (tipo == EnumsAtaque.Nieve) 
            {
                enumPlayerAtaques = EnumPlayerAtaques.LANZANDO;
                boolLanzo = false;
            }
            else if (tipo == EnumsAtaque.NORMAL)
            {
                enumPlayerAtaques = EnumPlayerAtaques.ATAQUENORMAL;

            }
        }
        public void Fisica(List<Objeto> listaDeObjetos, GameTime dataTime)
        {
            #region Colision Abajo
            // la gravedad y la friccion con la superficie
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.ABAJO))
            {
                //para retornar al caer
                posicionRetorno = posicion;

                // creando friccion
                if (enumPlayerEstadoX == EnumPlayerX.NORMAL)
                {

                    if (velocidad.X > 0)
                    {
                        if ((velocidad.X - friccion * (float)dataTime.ElapsedGameTime.TotalSeconds) < 0)
                            velocidad.X = 0;
                        else
                            velocidad.X -= friccion * (float)dataTime.ElapsedGameTime.TotalSeconds;

                    }
                    else if (velocidad.X < 0)
                    {
                        if ((velocidad.X + friccion * (float)dataTime.ElapsedGameTime.TotalSeconds) > 0)
                            velocidad.X = 0;
                        else
                            velocidad.X += friccion * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    }

                }
                    //para poder saber cuando no esta saltando
                    enumPlayerEstadoY = EnumPlayerY.NORMAL;

                    //para que no siga avanzando abajo
                    if (velocidad.Y > 0)
                    {
                        velocidad.Y = 0;
                        aceleracion.Y = 0;
                    }
            }           
            else 
            {
                aceleracion.Y += Juego.Gravedad * (float)dataTime.ElapsedGameTime.TotalSeconds;
                enumPlayerEstadoY = EnumPlayerY.SALTANDO;
            }
            #endregion

            #region Colision Arriba
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.ARRIBA))
            {
                if (aceleracion.Y < 0)
                {
                    aceleracion.Y = 0;
                }
                if (velocidad.Y < 0)
                {
                    velocidad.Y = 0;
                }
            }
            #endregion

            #region Colision Derecha
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.DERECHA))
            {
                if (aceleracion.X > 0)
                {
                    aceleracion.X = 0;
                }
                if (velocidad.X > 0)
                {
                    velocidad.X = 0;
                }
            }
            #endregion

            #region Colision Izquierda
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.IZQUIERDA))
            {
                if (aceleracion.X < 0)
                {
                    aceleracion.X = 0;
                }
                if (velocidad.X < 0)
                {
                    velocidad.X = 0;
                }
            }
            #endregion

        }

        public void IA()
        {
                if (-Juego.camara.Position.X + 800 > posicion.X)
                {
                    Personaje Objetivo = DetectarPlayerMasCerca(Juego.listaPersonajesVivos);

                    if (Objetivo.posicion.X - posicion.X > 0)
                    {
                        MoverDerecha();
                        if (Objetivo.posicion.Y < posicion.Y - 35 && enumPlayerEstadoY == EnumPlayerY.NORMAL)
                        {
                            Saltar();
                        }
                    }
                    else
                    {
                        MoverIzquierda();
                    }
                }
        }
        public void IABoss() 
        {
            if (-Juego.camara.Position.X + 800 > posicion.X)
            {
                Personaje Objetivo = DetectarPlayerMasCerca(Juego.listaPersonajesVivos);

                if (Objetivo.posicion.X - posicion.X > 0)
                {
                    MoverDerecha();
                    if (Objetivo.posicion.Y < posicion.Y - 35 && enumPlayerEstadoY == EnumPlayerY.NORMAL)
                    {
                        Saltar();
                    }
                }
                else
                {
                    MoverIzquierda();
                }

                //anular un poco la gravedad
                aceleracion.Y -= 15 * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;

                
                    if(Juego.teclado.IsKeyDown(Keys.Down))
                        Parir(posicion);
            }

        }
        public void Parir(Vector2 posicion) 
        {
            if (Juego.MapaActual.listaEnemigos.Count > 5) 
            {
                if (Juego.MapaActual.listaEnemigos[0].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[0].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                    Juego.MapaActual.listaEnemigos[0].velocidadMaximaPorSegundo -= 1; 
                }

                if (Juego.MapaActual.listaEnemigos[1].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[1].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                    Juego.MapaActual.listaEnemigos[1].velocidadMaximaPorSegundo -= 2; 
                }

                if (Juego.MapaActual.listaEnemigos[2].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[2].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                    Juego.MapaActual.listaEnemigos[2].velocidadMaximaPorSegundo -= 3; 
                }

                if (Juego.MapaActual.listaEnemigos[3].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[3].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                    Juego.MapaActual.listaEnemigos[3].velocidadMaximaPorSegundo -= 4; 
                }

                if (Juego.MapaActual.listaEnemigos[4].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[4].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                    Juego.MapaActual.listaEnemigos[4].velocidadMaximaPorSegundo += 1; 
                }

                if (Juego.MapaActual.listaEnemigos[5].enumTipo != Enumspersonaje.BOSS)
                {
                    Juego.MapaActual.listaEnemigos[5].posicion = new Vector2(posicion.X + 20, posicion.Y + 30);
                }
            }
        }

        public void VerificacionDanioConAtaques(List<Personaje> listaDePersonaje)
        {
            if (!inmortal)
            {
                foreach (Personaje i in listaDePersonaje)
                {
                    foreach (Ataque a in i.listaDeAtaque)
                    {
                        if (a.ColisionConmigo(rectangulo))
                        {
                            Health -= a.danio;
                            a.activo = false;
                            if (a.tipo == EnumsAtaque.Nieve)
                            {
                                if (CongelacionesPosibles > 0)
                                {
                                    congelado = true;
                                    if (enumTipo != Enumspersonaje.BOSS)
                                        tiempoCongelado = 0;
                                    else
                                        tiempoCongelado = 1.5f;
                                    CongelacionesPosibles--;
                                }
                            }
                        }
                    }
                }
            }
        }

        public Personaje DetectarPlayerMasCerca(List<Personaje> listaDePersonajes)  /// en prueba solo funciona para 2 
        {
            Personaje resultado = new Personaje(Enumspersonaje.ENEMIGO,posicion);

            if (listaDePersonajes.Count != 0)
            {
                resultado = listaDePersonajes[0]; // asignamos el primer personaje
            }

            foreach (Personaje i in listaDePersonajes) 
            {
                if(i.AlivePersonaje)
                {
                    if (Math.Sqrt(Math.Pow(i.posicion.X - posicion.X, 2) + Math.Pow(i.posicion.Y - posicion.Y, 2)) < Math.Sqrt(Math.Pow(resultado.posicion.X - posicion.X, 2) + Math.Pow(resultado.posicion.Y - posicion.Y, 2)))//Math.Abs(i.posicion.X - posicion.X) < Math.Abs(resultado.posicion.X - posicion.X)) 
                    {
                        resultado = i;
                    }
                }
            }

            return resultado;
        }

        public void ColisionPersonajeConListaDePersonaje(List<Personaje> lista)
        {
            if (!inmortal)
            {
                if (Colision(lista.ToList<Objeto>(), enumColision.TODAS))
                {
                    Health--;
                }
            }
        }
        public void ColisionaPowerUp()
        {
            foreach (PowerUp i in Juego.MapaActual.listaPowersUpDelMapa)
            {
                if (i.ColisionConmigo(rectangulo))
                {
                    if (i.activado) 
                    {
                        if (i.tipoPower == enumTipoPowerUp.CURACION) 
                        {
                            if (Health + i.vida < MaximaSaludPlayer)
                            {
                                Health += i.vida; //Curar
                                i.activado = false;
                                boolpowerUp = true;
                            }
                            else
                            {
                                Health = MaximaSaludPlayer;
                                i.activado = false;
                                boolpowerUp = true;
                            }
                            puntuacion += 75;
                        }
                        else if (i.tipoPower == enumTipoPowerUp.VelocidadPlus)
                        {
                            //Health += i.vida; //aumentar velocidad

                        }
                        else if (i.tipoPower == enumTipoPowerUp.ActivacionPower)
                        {
                            //Health += i.vida; // activar poder
                        }
                        else if (i.tipoPower == enumTipoPowerUp.Moneda)
                        {
                            monedas++;
                            puntuacion += 25;
                            i.activado = false;
                        }
                    }
                }
                
            }
        }

        public void CambiarDeArma() 
        {
            boolLanzo = true;
            if ((int)ataqueSelet == 2) // en vez de 1 va 2 esta asi para evitar crash
            {
                ataqueSelet = 0;
            }
            else
                ataqueSelet++;
        }
        public void Descongelar(GameTime dataTime) 
        {
            tiempoCongelado += (float)dataTime.ElapsedGameTime.TotalSeconds;

            if (tiempoCongelado < 1 ) 
            {
                Colorante = Color.DarkBlue;
            }
            else if (tiempoCongelado < 2) 
            {
                Colorante = Color.Blue;
            }
            else if (tiempoCongelado < 3)
            {
                Colorante = Color.CornflowerBlue;
            }
            else if (tiempoCongelado < 3.5)
            {
                Colorante = Color.LightSteelBlue;
            }
            else if (tiempoCongelado > 4)
            {
                congelado = false;
                Colorante = Color.White;
            }
        }
        public bool Inmortalidad() 
        {
            if (ataqueSelet == EnumsAtaque.Escudo && Juego.listLogros.Single(s => s.Nombre == ataqueSelet.ToString()).Activo)
                return true;
            else
                return false;
        }
    }
}
