﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public class Top
    {
        public Texture2D sprite;
        public Vector2 posicion;
        public Rectangle rectangulo;
        public float profundidad;
        public int numeroDePlayer;
        public float Salud;
        public Color colorante;

        public Top(int NumeroDePlayer, Color color)
        {
            sprite = Juego.topKiritoTexture;
            profundidad = 0f;
            numeroDePlayer = NumeroDePlayer;
            colorante = color;
        }

        public void Pintate(SpriteBatch batch, EnumsAtaque selet)
        {
            Juego.spriteBatch.Draw(sprite, rectangulo, null, colorante, 0f, Vector2.Zero, SpriteEffects.None, profundidad);

            
            //pintamos barra de vida
            PintarBarraDeVida(batch);


            //pintamos poderes con su selecion
            PintarPoderesConSelecion(batch,selet);

            //pintamos puntaje monedas y vidas
            PintarPuntajeDePersonajes(batch);

        }
        public void Actualizar(GameTime dataTime, float health )
        {
            posicion.X = -Juego.camara.Position.X + ((numeroDePlayer-1) * Juego.topKiritoTexture.Width) ; 
            rectangulo = new Rectangle((int)posicion.X,(int)posicion.Y,Juego.topKiritoTexture.Width,Juego.topKiritoTexture.Height);
            Salud = health;
        }

        public void PintarPuntajeDePersonajes(SpriteBatch batch) 
        {
            if(numeroDePlayer == 1)
            {
                batch.DrawString(Juego.Texto, ("Estrellas: " + Juego.takashi.monedas.ToString()), new Vector2(posicion.X,posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion: " + Juego.takashi.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width/3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 2)
            {
                batch.DrawString(Juego.Texto, ("Estrellas: " + Juego.takashi2.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion: " + Juego.takashi2.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi2.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 3)
            {
                batch.DrawString(Juego.Texto, ("Estrellas: " + Juego.takashi3.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion: " + Juego.takashi3.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi3.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
            else if (numeroDePlayer == 4)
            {
                batch.DrawString(Juego.Texto, ("Estrellas: " + Juego.takashi4.monedas.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height - 10), colorante);
                batch.DrawString(Juego.Texto, ("Puntuacion: " + Juego.takashi4.puntuacion.ToString()), new Vector2(posicion.X, posicion.Y + Juego.topKiritoTexture.Height + 5), colorante);
                batch.DrawString(Juego.Texto, (Juego.takashi4.Vidas.ToString()), new Vector2(posicion.X + (Juego.topKiritoTexture.Width / 3) + 5, posicion.Y), colorante);
            }
        }
        public void PintarBarraDeVida(SpriteBatch batch) 
        {
            //pintamos barra de abajo roja
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2) - 5, (int)posicion.Y + (Juego.topKiritoTexture.Height / 3), 100, 15), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.DarkRed, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.03f);
            //pintamos la cantidad de vida que tiene salud, color verde
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2) - 5, (int)posicion.Y + (Juego.topKiritoTexture.Height / 3), (int)(100 * ((double)Salud / 100)), 15), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.ForestGreen, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.02f);
            //pintamos el marco
            batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2) - 5, (int)posicion.Y + (Juego.topKiritoTexture.Height / 3), 100, 15), new Rectangle(0, 0, Juego.HealthBar.Width, 44), Color.White, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.01f);
        }
        public void PintarPoderesConSelecion(SpriteBatch batch,EnumsAtaque Selet) 
        {
            batch.Draw(Juego.topPoderesTextura, new Rectangle((int)posicion.X + (Juego.topKiritoTexture.Width / 2) - 5, (int)posicion.Y + (Juego.topKiritoTexture.Height / 3) + 15, 95, 32), null, colorante, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.01f);
            batch.Draw(Juego.marcoSeletTextura, new Rectangle(((int)posicion.X + (Juego.topKiritoTexture.Width / 2) - 5) + (32 * (int)Selet), (int)posicion.Y + (Juego.topKiritoTexture.Height / 3) + 15, 32, 32), null, colorante, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.02f);
            
        }
    }
}
