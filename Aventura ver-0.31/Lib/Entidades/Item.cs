﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;

namespace Lib.Entidades
{
    public class Item : Objeto
    {
        public string nombre;
        public int precio;
        public bool comprado = false;
        public bool disponible = false;

        public Item(int Precio,string Nombre, Texture2D Textura, Vector2 Posicion, bool Disponible)
        {
            precio = Precio;
            spriteTextura = Textura;
            posicion = Posicion;
            disponible = Disponible;
            nombre = Nombre;

            rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, spriteTextura.Width, spriteTextura.Height);

        }

        public void Actualizar()
        {
            rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, spriteTextura.Width, spriteTextura.Height);

            if (disponible)
                Colorante = Color.White;
            else
                Colorante = Color.Gray;
        }

        public void Pintate(SpriteBatch batch)
        {
            //imagen
            batch.Draw(spriteTextura, posicion, null, Colorante, 0f, Vector2.Zero, 1f, SpriteEffects.None, profundidad);
            //Precio
            batch.DrawString(Juego.Texto, precio.ToString(), new Vector2(posicion.X + 20, posicion.Y+ spriteTextura.Height), Colorante, 0f, Vector2.Zero, 1f, SpriteEffects.None, profundidad);
            //Nombre
            batch.DrawString(Juego.Texto, nombre, new Vector2(posicion.X + (spriteTextura.Width/2 - 30), posicion.Y + 3 ), Colorante, 0f, Vector2.Zero, 1f, SpriteEffects.None, profundidad);
        }

        public void Comprar() 
        {
            comprado = true;
            Juego.monedasGeneral -= precio;

            if(nombre != "Vida")
                Juego.listLogros.Single(s => s.Nombre == nombre).Activo = true;
            disponible = false;

            #region Casos Especiales
            switch (nombre)
            {
                case "DOBLEFUEGO":
                    break;

                case "BOLADENIEVE":
                    break;

                case "RAYO":
                    break;

                case "Final":
                    break;

                case "Vida":
                    disponible = true;

                    Juego.takashi.Vidas++;
                    Juego.takashi2.Vidas++;
                    Juego.takashi3.Vidas++;
                    Juego.takashi4.Vidas++;
                    break;


                default:
                    break;
            }
            #endregion

        }

    }
}
