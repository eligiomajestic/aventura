﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public enum EnumsAtaque { Fuego, Nieve, Escudo, NORMAL }
    public class Ataque : Objeto
    {
        public Animacion animacion;
        public EnumsAtaque tipo;
        public int danio;
        public bool activo;
        public bool mirandoDerecha;
        public Vector2 aceleracion;
        public Vector2 velocidad;
        public float tiempo = 0;

        public Ataque(Vector2 Posicion, EnumsAtaque TipodeAtaque , bool MirandoDerecha,Vector2 VelocidadDelTirador) 
        {
            this.posicion = Posicion;
            tipo = TipodeAtaque;
            mirandoDerecha = MirandoDerecha;
            activo = true;

            if (tipo == EnumsAtaque.Fuego) 
            {
                danio = 20;
                tamanio = new Vector2(50,50);
                if (mirandoDerecha)
                    velocidad = new Vector2(0 + (VelocidadDelTirador.X *100),0);
                else
                    velocidad = new Vector2(0 + (VelocidadDelTirador.X *100), 0);
                aceleracion = new Vector2(2000,0);
                profundidad = 0.4f;

                animacion = new Animacion(Juego.graphics, Posicion);
                animacion.adicionarImagen(Juego.AtaqueDobleFuego, 6);
                animacion.iniciar(0, 5, 0.3f, true);
            }
            else if (tipo == EnumsAtaque.Nieve) 
            {
                danio = 8;
                tamanio = new Vector2(50, 50);
                if(mirandoDerecha)
                    velocidad = new Vector2(400 + (VelocidadDelTirador.X * 20), (VelocidadDelTirador.Y*20) -300);
                else
                    velocidad = new Vector2(-400 + (VelocidadDelTirador.X *20), (VelocidadDelTirador.Y*20)  - 300);
                aceleracion = new Vector2(0,0);
                profundidad = 0f;

                animacion = new Animacion(Juego.graphics, Posicion);
                animacion.adicionarImagen(Juego.AtaqueBolaDeNieveTextura, 12);
                animacion.iniciar(0, 11, 0.2f, true);
            }
            else if (tipo == EnumsAtaque.NORMAL)
            {
                danio = 50;
                tamanio = new Vector2(50,75);
                velocidad = Vector2.Zero;
                aceleracion = new Vector2(0, 0);
                profundidad = 0f;
            }
        }

        public void Actualizar(GameTime dataTime)
        {
            if (tipo == EnumsAtaque.NORMAL)
            {
                if (mirandoDerecha)
                {
                    rectangulo = new Rectangle((int)posicion.X + 30, (int)posicion.Y, (int)tamanio.X, (int)tamanio.Y);
                }
                else
                {
                    rectangulo = new Rectangle((int)posicion.X - ((int)tamanio.X- 10), (int)posicion.Y, (int)tamanio.X, (int)tamanio.Y);
                }

                if (tiempo < 0.3f)
                {
                    tiempo += (float)dataTime.ElapsedGameTime.TotalSeconds;
                }
                else 
                {
                    activo = false;
                }
            }
            else
            {
                #region Global
                if (activo)
                {
                    animacion.Actualizar(dataTime, mirandoDerecha);

                    if (mirandoDerecha)
                    {
                        velocidad.X += aceleracion.X * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    }
                    else
                    {
                        velocidad.X -= aceleracion.X * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    }
                    velocidad.Y += aceleracion.Y * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    posicion += velocidad * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    animacion.ponerPosicion(posicion.X, posicion.Y);

                    rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, (int)tamanio.X, (int)tamanio.Y);
                }
                else
                {
                    rectangulo = Rectangle.Empty;
                }
                #endregion

                #region Bola De Nive
                if (tipo == EnumsAtaque.Nieve && activo)
                {
                    aceleracion.Y += (Juego.Gravedad * (float)dataTime.ElapsedGameTime.TotalSeconds) * 40;
                }
                #endregion  // COMO UNA PEDRA LAIGA
            }
        }
        public void Pintate(SpriteBatch batch)
        {
            if (activo && tipo != EnumsAtaque.NORMAL)
            {
                if (tipo != EnumsAtaque.NORMAL)
                {
                    animacion.Dibujar(batch, profundidad, Colorante);
                }
            }
        }
    }
}
