﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Lib.Entidades
{
    public class Mapa
    {
        public int escalaX;
        public int escalaY;
        public int[,] matrizobstaculos;
        public int numeroDelMapa;

        public string DirecionDelMapa;

        public Texture2D fondo;

        public List<PowerUp> listaPowersUpDelMapa;
        public List<Objeto> listaColisionMapa;
        public List<Personaje> listaEnemigos;
        public List<Objeto> listaAdornos;
        public List<Personaje> listaNPC;

        public Mapa(int NumeroDelMapa , enumDificultad difi) 
        {
            Juego.peleandoConBoss = false;
            Inicializarlistas();
            matrizobstaculos = new int [12,250];
            fondo = Juego.fondo1;

            escalaX = 32;
            escalaY = 50;

            numeroDelMapa = NumeroDelMapa;

            if (numeroDelMapa == 1) 
                DirecionDelMapa = "Content/Archivos/Piso.txt";
            else if (numeroDelMapa == 2)
                DirecionDelMapa = "Content/Archivos/Piso2.txt";
            else if (numeroDelMapa == 3)
                DirecionDelMapa = "Content/Archivos/Piso3.txt";
            else if (numeroDelMapa == 4)
                DirecionDelMapa = "Content/Archivos/Piso4.txt";
            else if (numeroDelMapa == 5)
                DirecionDelMapa = "Content/Archivos/Piso5.txt";

            LlenarMatrizObstaculos();
            LlenarListas();

            CambioDeDificultad(difi);

            //inicializamos los enemigos y npc luego de cargar el mapa
            InicializarAnimacionesDeEnemigosYNPC();

            //cargamos las animaciones de npc y enemigos
            CargarAnimacionesDeEnemigosYNPC();
                    
        }

        public void Actualizar(GameTime dateTime) 
        {

            
            //actualizamos Enemigos
            foreach (Personaje i in listaEnemigos)
            {
                i.Actualizar(dateTime);
            }
            //actualizamos NPC
            foreach (Personaje i in listaNPC)
            {
                i.Actualizar(dateTime);
            }

            //actualizamos Adornos
            foreach (Objeto i in listaAdornos)
            {
                i.Actualizar(dateTime);
            }

            //actualizamos powerup NUEVO

            foreach (PowerUp i in listaPowersUpDelMapa)
            {
                 i.Actualizar(dateTime);
            }



        }
        public void Pintate(SpriteBatch batch) 
        {
            //NUEVOO
            foreach (PowerUp i in Juego.MapaActual.listaPowersUpDelMapa)
            {
                    i.Pintate(batch);
            }

            foreach (Objeto i in listaColisionMapa)
            {
                i.Pintate(batch);
            }

            foreach (Objeto i in listaAdornos)
            {
                i.Pintate(batch);
            }

            
            foreach (Personaje i in listaEnemigos)
            {
                if (i.AlivePersonaje)
                    i.Pintate(batch);
            }

            PintarFondos(batch);
        }

        public void LlenarMatrizObstaculos()
        {
            char[] letras = new char[250];
            string linea;

            StreamReader reader = new StreamReader(DirecionDelMapa);
            {
                for (int j = 0; j < 12; j++)
                {
                    linea = reader.ReadLine();
                    letras = linea.ToCharArray();
                    for (int i = 0; i < letras.Length; i++)
                    {
                        if (letras[i] == 'x')
                        {
                            matrizobstaculos[j, i] = 2;
                        }
                        else if (letras[i] == 'i')
                        {
                            matrizobstaculos[j, i] = 1;
                        }
                        else if (letras[i] == 'g')
                        {
                            matrizobstaculos[j, i] = 8;
                        }
                        else if (letras[i] == 'b')
                        {
                            matrizobstaculos[j, i] = 100;
                        }
                        else if (letras[i] == 'f')
                        {
                            matrizobstaculos[j, i] = 3;
                        }
                        else if (letras[i] == 'o')
                        {
                            matrizobstaculos[j, i] = 0;
                        }

                        else if (letras[i] == 'n')
                        {
                            matrizobstaculos[j, i] = 4;
                        }
                        else if (letras[i] == 'q') 
                        {
                            matrizobstaculos[j, i] = 5;
                        }
                        else if (letras[i] == 'h')
                        {
                            matrizobstaculos[j, i] = 6;
                        }
                        else if (letras[i] == 'w')
                        {
                            matrizobstaculos[j, i] = 7;
                        }
                        else if (letras[i] == 'e')
                        {
                            matrizobstaculos[j, i] = 20;
                        }
                        else if (letras[i] == 'p')
                        {
                            matrizobstaculos[j, i] = 10;
                        }
                        else if (letras[i] == 'm')
                        {
                            matrizobstaculos[j, i] = 11;
                        }
                    }
                    
                }
            }

        }

        //inicializar listas
        public void Inicializarlistas()
        {
            this.listaPowersUpDelMapa = new List<PowerUp>();
            this.listaColisionMapa = new List<Objeto>();
            this.listaEnemigos = new List<Personaje>();
            this.listaAdornos = new List<Objeto>();
            this.listaNPC = new List<Personaje>();
        }

        //llenando matrices
        public void LlenarListas()
{
            for (int j = 0; j < 12; j++)
            {
                for (int i = 0; i < 250; i++)
                {
                    switch (matrizobstaculos[j,i])
                    {
                            //piso = 1
                        case 1:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBlockInicio, new Vector2(i * escalaX, j * escalaY)));
                            break;

                        //piso = 2
                        case 2:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBlock, new Vector2(i * escalaX, j * escalaY)));
                            break;

                        //piso = 3
                        case 3:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBlockFin, new Vector2(i * escalaX, j * escalaY)));
                            break;

                        case 8:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBlock5, new Vector2(i * escalaX, j * escalaY)));
                            break;
                        //nubes = 4
                        case 4:
                            listaAdornos.Add(new Objeto(Juego.texturaNube, new Vector2(i * escalaX, j * escalaY -(10+ (j*10)))));
                            break;
                        //block de nubes
                        case 5:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBloqueNube, new Vector2(i * escalaX, j * escalaY)));
                            break;
                        //bloque de hierba
                        case 6:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBloqueHierba, new Vector2(i * escalaX, j * escalaY)));
                            break;
                        case 7:
                            listaColisionMapa.Add(new Objeto(Juego.texturaBloqueMadera, new Vector2(i * escalaX, j * escalaY)));
                            break;
                        //powerUp = 10
                        case 10:
                            listaPowersUpDelMapa.Add(new PowerUp(new Vector2(i * escalaX, j * escalaY),enumTipoPowerUp.CURACION));
                            break;
                        //Monedas
                        case 11:
                            listaPowersUpDelMapa.Add(new PowerUp(new Vector2(i * escalaX, j * escalaY), enumTipoPowerUp.Moneda));
                            break;

                        //enemigo normal = 20
                        case 20:
                            listaEnemigos.Add(new Personaje(Enumspersonaje.ENEMIGO, new Vector2(i * escalaX, j * escalaY)));
                            break;

                            //boss
                        case 100:
                            listaEnemigos.Add(new Personaje(Enumspersonaje.BOSS, new Vector2(i * escalaX, j * escalaY)));
                            break;

                        default:
                            break;

                    }
                }
            }
        }

        public void CambioDeDificultad(enumDificultad dif)
        {
            foreach (Personaje i in listaEnemigos)
            {
                i.Health = i.Health * ((int)Juego.Dificultad + 1);
                i.MaximaSaludPlayer = i.MaximaSaludPlayer * ((int)Juego.Dificultad + 1);
            }
        }

        public void InicializarAnimacionesDeEnemigosYNPC()
        {
            foreach (Personaje i in listaEnemigos)
            {
                i.InicializarAnimaciones();
            }

            foreach (Personaje i in listaNPC)
            {
                i.InicializarAnimaciones();
            }
        }

        public void CargarAnimacionesDeEnemigosYNPC()
        {
            foreach (Personaje i in listaEnemigos)
            {
                i.CargarAnimaciones();
            }

            foreach (Personaje i in listaNPC)
            {
                i.CargarAnimaciones();
            }
        }

        public void PintarFondos(SpriteBatch batch) 
        {
            //aqui pintamos fondos diferentes segun el mapa
            switch (numeroDelMapa)
            {
                case 1:
                    batch.Draw(Juego.fondo3, new Rectangle(0, 0, Juego.fondo3.Width, Juego.fondo3.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo3, new Rectangle(Juego.fondo3.Width, 0, Juego.fondo3.Width, Juego.fondo3.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo3, new Rectangle(-Juego.fondo3.Width, 0, Juego.fondo3.Width, Juego.fondo3.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo3, new Rectangle(2 * Juego.fondo3.Width, 0, Juego.fondo3.Width, Juego.fondo3.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    break;
                case 2:
                    batch.Draw(Juego.fondo2, new Rectangle(0, 0, Juego.fondo2.Width, Juego.fondo2.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo2, new Rectangle(Juego.fondo2.Width, 0, Juego.fondo2.Width, Juego.fondo2.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo2, new Rectangle(-Juego.fondo2.Width, 0, Juego.fondo2.Width, Juego.fondo2.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo2, new Rectangle(2 * Juego.fondo2.Width, 0, Juego.fondo2.Width, Juego.fondo2.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    break;
                case 3:
                    //estos son lo que van asi
                    batch.Draw(Juego.fondo1, new Rectangle(0, 0, Juego.fondo1.Width, Juego.fondo1.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo1, new Rectangle(Juego.fondo1.Width, 0, Juego.fondo1.Width, Juego.fondo1.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo1, new Rectangle(-Juego.fondo1.Width, 0, Juego.fondo1.Width, Juego.fondo1.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo1, new Rectangle(2 * Juego.fondo1.Width, 0, Juego.fondo1.Width, Juego.fondo1.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    break;
                case 4:
                    batch.Draw(Juego.fondo4, new Rectangle(0, 0, Juego.fondo4.Width, Juego.fondo4.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo4, new Rectangle(Juego.fondo4.Width, 0, Juego.fondo4.Width, Juego.fondo4.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo4, new Rectangle(-Juego.fondo4.Width, 0, Juego.fondo4.Width, Juego.fondo4.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo4, new Rectangle(2 * Juego.fondo4.Width, 0, Juego.fondo4.Width, Juego.fondo4.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    break;
                case 5:
                    batch.Draw(Juego.fondo5, new Rectangle(0, 0, Juego.fondo5.Width, Juego.fondo5.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo5, new Rectangle(Juego.fondo5.Width, 0, Juego.fondo5.Width, Juego.fondo5.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo5, new Rectangle(-Juego.fondo5.Width, 0, Juego.fondo5.Width, Juego.fondo5.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    batch.Draw(Juego.fondo5, new Rectangle(2 * Juego.fondo5.Width, 0, Juego.fondo5.Width, Juego.fondo5.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 1f);
                    break;

                default:
                    break;
            }
            
        }
    }
}
