﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Recursos;

namespace Lib.Entidades
{
    public enum Enumspersonaje {PLAYER, ENEMIGO, ADORNO}
    public enum EnumPlayerX {CAMINANDOderecha,CAMINANDOizquierda,NORMAL, MUERTO}
    public enum EnumPlayerY { SALTANDO, NORMAL,}
    public enum EnumPlayerAtaques {NINGUNO,LANZANDO}


    public class Personaje : Objeto
    {
        public List<Ataque> listaDeAtaque;

        private GameTime dataTimePersonaje;
        public Animacion aniActual;

        public bool boolLanzo;
        public bool boolpowerUp;

        public Animacion aniStandar;
        public Animacion aniCaminando;
        public Animacion aniSalto;
        public Animacion aniMuertePersonaje;
        public Animacion aniAtaqueLanzador;
        public Animacion aniPowerUp;

        public bool mirandoDeracha;
        // Variables booleanas para saber cuando un player o un enemigo estan muertos o vivos
        public bool AlivePersonaje;
        public Enumspersonaje enumTipo;
        public EnumPlayerX enumPlayerEstadoXUltimo;
        public EnumPlayerAtaques enumPlayerAtaques;
        public EnumPlayerX enumPlayerEstadoX;
        public EnumPlayerY enumPlayerEstadoY;

        //Vidas del jugador
        public int Vidas;

        //Salud del jugador y enemigo
        public int Health;
        public int numeroDePlayer;
        public int Tiempo;
        public int MaximaSaludPlayer;

        public float velocidadMaximaPorSegundo;
        public float escalaVelocidad;
        public float escalaSalto;
        public float friccion;
        public Vector2 velocidad;
        public Vector2 aceleracion;
        

        public Personaje(Enumspersonaje TIPO,Vector2 Posicion, int NumerePlayer = 1)
        {
            
            listaDeAtaque = new List<Ataque>();
            posicion = Posicion;
            numeroDePlayer = NumerePlayer;
            aceleracion = Vector2.Zero;
            velocidad = Vector2.Zero;
            velocidadMaximaPorSegundo = 5;
            enumTipo = TIPO;
            
            escalaVelocidad = 50;
            escalaSalto = 250;
            friccion = 50f;
            mirandoDeracha = true;
            Tiempo = 0;
            AlivePersonaje = true;
            boolLanzo = true;
            enumPlayerAtaques = EnumPlayerAtaques.NINGUNO;
            boolpowerUp = false;

            if (TIPO == Enumspersonaje.PLAYER)
            {
                tamanio = new Vector2(45, 55);
                profundidad = 0.4f;
                enumPlayerEstadoX = EnumPlayerX.NORMAL;
                enumPlayerEstadoY = EnumPlayerY.NORMAL;
                //Vidas inicializadas a 3
                Vidas = 3;
                Health = 100;
                MaximaSaludPlayer = 100;
                

            }
            else if (TIPO == Enumspersonaje.ENEMIGO) 
            {
                profundidad = 0.4f;
                tamanio = new Vector2(45, 55);
                enumPlayerEstadoX = EnumPlayerX.NORMAL;
                Vidas = 1;
                Health = 50;
                MaximaSaludPlayer = 50;
            }
        }

        public override void Actualizar(GameTime dataTime)
        {
            
            this.dataTimePersonaje = dataTime;
            switch (enumTipo)
            {
                case Enumspersonaje.PLAYER:
                    #region Personaje

<<<<<<< HEAD
                    Tiempo += dataTime.ElapsedGameTime.Milliseconds;
                    if (Tiempo <= 200 )
                    {
                        Tiempo = 0;
                    }

                    //Mata el personaje si sale de pantalla hacia abajo
                    MuertePorCaidaFueraDePantalla();
               
                    //Si el jugador intersecta un oponente, pierde un punto de vida - prueba
                    ColisionPersonajeConListaDePersonaje(Juego.MapaActual.listaEnemigos);

=======
>>>>>>> 3c4c93541c7375fd30052696056463ef69ad1374
                    //Actualizamos la barra de salud
                    ActualizarBarraDeSaludPlayer(dataTime);

                    //Aplicammos la fisica
                    Fisica(Juego.MapaActual.listaColisionMapa, dataTime);

                    //actualizamos la velocidad
                    velocidad += aceleracion * (float)dataTime.ElapsedGameTime.TotalSeconds;

                    //limitador de velocidad
                    LimitesDeVelocidad();

                    // linitamos posicion al personaje dependiendo de la camara
                    LimitesDePosicion();

                    //Cambiamos posicion
                    posicion += velocidad;                                

                    //actualizamos rectangulo
                    rectangulo = new Rectangle((int)posicion.X,(int)posicion.Y,(int)tamanio.X,(int)tamanio.Y);

                    //Si el jugador intersecta un oponente, pierde un punto de vida - prueba
                    ColisionPersonajeConListaDePersonaje(Juego.MapaActual.listaEnemigos);

                    //Verificamos colision con power ups
                    ColisionaPowerUp();

                    //Verificamos si recibe daño
                    VerificacionDanioConAtaques(Juego.MapaActual.listaEnemigos);

                    //Llamamos al Actualizar de las animacines
                    AnimacionActualizar(dataTime);

                    //ARIEL  QUE HACE ESTO??????  COMENTA
                    Tiempo += dataTime.ElapsedGameTime.Milliseconds;
                    if (Tiempo <= 200 )
                    {
                        Tiempo = 0;
                    }

                    break;
                    #endregion

                case Enumspersonaje.ENEMIGO:
                    #region Enemigo

                    //Actualizamos la barra de salud
                    ActualizarBarraDeSaludPlayer(dataTimePersonaje);

                    //Aplicammos la fisica
                    Fisica(Juego.MapaActual.listaColisionMapa, dataTime);

                    //actualizamos la aceleracion
                    velocidad += aceleracion * (float)dataTime.ElapsedGameTime.TotalSeconds;

                    //limitador de velocidad
                    LimitesDeVelocidad();

                    //Cambiamos posicion
                    posicion += velocidad;

                    //actualizamos rectangulo
                    rectangulo = new Rectangle((int)posicion.X, (int)posicion.Y, (int)tamanio.X, (int)tamanio.Y);

                    //Llamamos al Actualizar de las animacines
                    AnimacionActualizar(dataTime);

                    //Verificamos si recibe daño
                    VerificacionDanioConAtaques(Juego.listaDePersonaje);

                    if (AlivePersonaje)
                    {
                        IA();
                    }
                    else
                    {
                        rectangulo = Rectangle.Empty;
                    }


                    break;
                    #endregion

                case Enumspersonaje.ADORNO:
                    #region Adorno
                    break;
                    #endregion

            }

            //Si esta vivo, mira su HP y dime si puedo matarlo >:D
            if (AlivePersonaje)
            {
                ActualizarVivoOMuerto();
            }

            //actualizamos Ataques
            foreach (Ataque i in listaDeAtaque)
            {
                i.Actualizar(dataTime);
            }

            //Actualizamos la animacion
            aniActual.Actualizar(dataTime,mirandoDeracha);
        }
        public override void Pintate(SpriteBatch batch)
        {

            aniActual.Dibujar(batch, profundidad);
            
            //actualizamos Ataques
            foreach (Ataque i in listaDeAtaque)
            {
                i.Pintate(batch);
            }

            if (AlivePersonaje)
            {
                //pintamos barra de abajo griss
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10, MaximaSaludPlayer, 10), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.Gray, 0f, Vector2.Zero, SpriteEffects.None, profundidad +0.03f);
                //pintamos la cantidad de vida que tiene salud
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10, (int)(MaximaSaludPlayer * ((double)Health / MaximaSaludPlayer)), 10), new Rectangle(0, 45, Juego.HealthBar.Width, 44), Color.Red, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.02f);
                //pintamos el marco
                batch.Draw(Juego.HealthBar, new Rectangle((int)posicion.X, (int)posicion.Y - 10, MaximaSaludPlayer, 10), new Rectangle(0, 0, Juego.HealthBar.Width, 44), Color.White, 0f, Vector2.Zero, SpriteEffects.None, profundidad + 0.01f);
            }
        }

        public void InicializarAnimaciones() 
        {
            aniActual = new Animacion(Juego.graphics, posicion);
            aniStandar = new Animacion(Juego.graphics, posicion);
            aniCaminando = new Animacion(Juego.graphics, posicion);
            aniSalto = new Animacion(Juego.graphics, posicion);
            aniMuertePersonaje = new Animacion(Juego.graphics, posicion);
            aniAtaqueLanzador = new Animacion(Juego.graphics, posicion);
            aniPowerUp = new Animacion(Juego.graphics, posicion);
        }
        public void AnimacionActualizar(GameTime dataTime)
        {   
            //Aqui comprobamos si el jugador esta con vida para ejecutar las siguientes animaciones
            if (AlivePersonaje)
            {
                if (!boolpowerUp)
                {
                    if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
                    {
                        if (enumPlayerAtaques == EnumPlayerAtaques.NINGUNO)
                        {//aqui esta en el piso y no esta atacando
                            switch (enumPlayerEstadoX)
                            {
                                case EnumPlayerX.NORMAL:
                                    aniActual = aniStandar;
                                    break;
                                case EnumPlayerX.CAMINANDOderecha:
                                    aniActual = aniCaminando;
                                    break;
                                case EnumPlayerX.CAMINANDOizquierda:
                                    aniActual = aniCaminando;
                                    break;
                            }
                        }
                        else
                        {
                            //aqui esta en el piso y esta atacando
                            switch (enumPlayerAtaques)
                            {
                                case EnumPlayerAtaques.LANZANDO:
                                    aniActual = aniAtaqueLanzador;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        if (enumPlayerAtaques == EnumPlayerAtaques.NINGUNO)
                        {
                            aniActual = aniSalto;
                        }
                        else
                        {
                            //aqui esta en el Aire y esta atacando ojo solo ataques que puedan tirar en el aire
                            switch (enumPlayerAtaques)
                            {
                                case EnumPlayerAtaques.LANZANDO:
                                    aniActual = aniAtaqueLanzador;
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    aniActual = aniPowerUp;   
                }

            }
              //Si entra aqui quiere decir que el booleano era falso, por lo que el jugador esta muerto :( 
            //entonces entra la animacion de muerte del personaje, que se ejecuta una sola vez
            else
            {
                aniActual = aniMuertePersonaje;
            }

            aniActual.Actualizar(dataTime, mirandoDeracha);
            reiniciarAnimacionesNoRepetible();
            aniActual.ponerPosicion(posicion.X, posicion.Y);

        }
        public void CargarAnimaciones() 
        {
            switch (enumTipo)
            {
                case Enumspersonaje.PLAYER:
                    // animacion standar
                    aniStandar.adicionarImagen(Juego.PlayerTexturaStandar, 4);
                    aniStandar.iniciar(0, 3, 1f, true);

                    //animacion caminando
                    aniCaminando.adicionarImagen(Juego.PlayerTexturaCaminando, 3);
                    aniCaminando.iniciar(0, 2, 0.7f, true);

                    //animacion caminando
                    aniSalto.adicionarImagen(Juego.PlayerTexturaSaltando, 2);
                    aniSalto.iniciar(0, 1, 1f, true);

                    //animacion tomando power up
                    aniPowerUp.adicionarImagen(Juego.PlayerTexturaPowerUp, 6);
                    aniPowerUp.iniciar(0, 5, 1.2f, false);

                    //animacion muerte
                    aniMuertePersonaje.adicionarImagen(Juego.PlayerTexturaMuerte, 4);
                    aniMuertePersonaje.iniciar(0, 3, 1.2f, false);


                    //animacion Lanzador
                    aniAtaqueLanzador.adicionarImagen(Juego.AtaqueLanzador, 8);
                    aniAtaqueLanzador.iniciar(0, 7, 1f, false);
                    break;

                case Enumspersonaje.ENEMIGO:

                        // animacion standar
                        aniStandar.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniStandar.iniciar(0, 4, 1f, true);

                        //animacion caminando
                        aniCaminando.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniCaminando.iniciar(0, 4, 1f, true);

                        //animacion caminando
                        aniSalto.adicionarImagen(Juego.EnemigoNormalTexturaStandar, 5);
                        aniSalto.iniciar(0, 4, 1f, true);

                    break;

                case Enumspersonaje.ADORNO:
                    break;
            }
        }
        public void reiniciarAnimacionesNoRepetible()
        {
            if (boolpowerUp) 
            {
                if (aniActual.frames == 5)
                {
                    boolpowerUp = false;
                    aniActual.frames = 0;
                }
            }
            else
            {
                if (enumPlayerAtaques == EnumPlayerAtaques.LANZANDO)
                {
                    if (aniActual.frames == 7)
                    {
                        enumPlayerAtaques = EnumPlayerAtaques.NINGUNO;
                        aniActual.frames = 0;
                        boolLanzo = true;
                    }
                }
            }
        }

        public void Saltar()
        {
            aceleracion.Y -= escalaSalto * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
            velocidad.Y += aceleracion.Y;
            aceleracion.Y = 0;
            enumPlayerEstadoY = EnumPlayerY.SALTANDO;
            
        }
        public void MoverIzquierda()
        {
            if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
            {
                aceleracion.X -= escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
                enumPlayerEstadoX = EnumPlayerX.CAMINANDOizquierda;
            }
            else 
            {
                aceleracion.X -= (escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds)/2;
                enumPlayerEstadoX = EnumPlayerX.CAMINANDOizquierda;
            }
            //aqui volteamos al jugador
            mirandoDeracha = false;
        }
        public void MoverDerecha()
        {
            if (enumPlayerEstadoY == EnumPlayerY.NORMAL)
            {
                aceleracion.X += escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds;
                enumPlayerEstadoX = EnumPlayerX.CAMINANDOderecha;
            }
            else 
            {
                aceleracion.X += (escalaVelocidad * (float)dataTimePersonaje.ElapsedGameTime.TotalSeconds)/2;
                enumPlayerEstadoX = EnumPlayerX.CAMINANDOderecha;
            }
            //aqui volteamos al jugador
            mirandoDeracha = true;
        }

        //Funcion para matar personajes :S
        public void ActualizarVivoOMuerto()
        {
            if(Health <= 0)
            {
                AlivePersonaje = false;
            }

        }
        public void ActualizarBarraDeSaludPlayer(GameTime gameTime)
        {
            Health = (int)MathHelper.Clamp(Health, 0, 100);
        }
        //Funcion que me permite ver si se sale de la pantalla hacia abajo, si tiene vidas restale 1 y colocalo en posicion inicial
        public void MuertePorCaidaFueraDePantalla()
        {
            if (posicion.Y > Juego.fondo1.Height && Vidas > 0)
            {
                posicion = new Vector2(300, 300);
                Vidas -= 1;
            }
            else if (posicion.Y > Juego.fondo1.Height && Vidas == 0)
            {
                //Poner logica de gameState gameover aqui
            }
        }

        public void LimitesDeVelocidad()
        {
            if (enumPlayerEstadoXUltimo != enumPlayerEstadoX && enumPlayerEstadoX != EnumPlayerX.NORMAL) 
            {
                aceleracion.X = 0;
            }

            if(velocidad.X < -velocidadMaximaPorSegundo && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
            {
                velocidad.X = -velocidadMaximaPorSegundo;
            }

            if (velocidad.X > velocidadMaximaPorSegundo && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
            {
                velocidad.X = velocidadMaximaPorSegundo;
            }

            if (velocidad.Y > 20)
            {
                velocidad.Y = 20;
            }

            enumPlayerEstadoXUltimo = enumPlayerEstadoX;

            //prueba
            if (Math.Abs(velocidad.X) > velocidadMaximaPorSegundo) 
            {

            }
        }
        public void LimitesDePosicion() 
        {
            if (posicion.X > -(Juego.camara.Position.X) + Juego.graphics.GraphicsDevice.Viewport.Width - 100) 
            {
                if (velocidad.X > 0)
                    velocidad.X = 0;
            }
            else if (posicion.X < -(Juego.camara.Position.X) )
            {
                if (velocidad.X < 0)
                    velocidad.X = 0;
            }
        }
        public void AnularAceleracion(KeyboardState teclado) 
        {
            switch(numeroDePlayer)
            {
                case 1:
                    if (teclado.IsKeyUp(Keys.Left) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                    if (teclado.IsKeyUp(Keys.Right) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 2:
                    if (teclado.IsKeyUp(Keys.A) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                    if (teclado.IsKeyUp(Keys.D) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 3:
                if (teclado.IsKeyUp(Keys.F) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                if (teclado.IsKeyUp(Keys.H) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                    {
                        enumPlayerEstadoX = EnumPlayerX.NORMAL;
                        aceleracion.X = 0;
                    }
                break;

                case 4:
                if (teclado.IsKeyUp(Keys.J) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOizquierda)
                {
                    enumPlayerEstadoX = EnumPlayerX.NORMAL;
                    aceleracion.X = 0;
                }
                if (teclado.IsKeyUp(Keys.L) && enumPlayerEstadoX == EnumPlayerX.CAMINANDOderecha)
                {
                    enumPlayerEstadoX = EnumPlayerX.NORMAL;
                    aceleracion.X = 0;
                }
                break;
            }
        }

        public void Atacar(EnumsAtaque tipo)
        {
            listaDeAtaque.Add(new Ataque(posicion,tipo,mirandoDeracha));

            if (tipo == EnumsAtaque.DOBLEFUEGO) 
            {
                enumPlayerAtaques = EnumPlayerAtaques.LANZANDO;
                boolLanzo = false;
            }
        }
        public void Fisica(List<Objeto> listaDeObjetos, GameTime dataTime)
        {
            #region " colision abajo"
            // la gravedad y la friccion con la superficie
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.ABAJO))
            {
                // creando friccion
                if (enumPlayerEstadoX == EnumPlayerX.NORMAL)
                {
                    if (velocidad.X > 0)
                    {
                        if ((velocidad.X - friccion * (float)dataTime.ElapsedGameTime.TotalSeconds) < 0)
                            velocidad.X = 0;
                        else
                            velocidad.X -= friccion * (float)dataTime.ElapsedGameTime.TotalSeconds;

                    }
                    else if (velocidad.X < 0)
                    {
                        if ((velocidad.X + friccion * (float)dataTime.ElapsedGameTime.TotalSeconds) > 0)
                            velocidad.X = 0;
                        else
                            velocidad.X += friccion * (float)dataTime.ElapsedGameTime.TotalSeconds;
                    }

                }
                    //para poder saber cuando no esta saltando
                    enumPlayerEstadoY = EnumPlayerY.NORMAL;

                    //para que no siga avanzando abajo
                    if (velocidad.Y > 0)
                    {
                        velocidad.Y = 0;
                        aceleracion.Y = 0;
                    }
            }           
            else 
            {
                aceleracion.Y += Juego.Gravedad * (float)dataTime.ElapsedGameTime.TotalSeconds;
                enumPlayerEstadoY = EnumPlayerY.SALTANDO;
            }
            #endregion

            #region "Colision arriba"
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.ARRIBA))
            {
                if (aceleracion.Y < 0)
                {
                    aceleracion.Y = 0;
                }
                if (velocidad.Y < 0)
                {
                    velocidad.Y = 0;
                }
            }
            #endregion

            #region "Colision derecha"
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.DERECHA))
            {
                if (aceleracion.X > 0)
                {
                    aceleracion.X = 0;
                }
                if (velocidad.X > 0)
                {
                    velocidad.X = 0;
                }
            }
            #endregion

            #region "Colision izquierda"
            if (this.Colision(Juego.MapaActual.listaColisionMapa, enumColision.IZQUIERDA))
            {
                if (aceleracion.X < 0)
                {
                    aceleracion.X = 0;
                }
                if (velocidad.X < 0)
                {
                    velocidad.X = 0;
                }
            }
            #endregion

        }

        public void IA()
        {
                if (-Juego.camara.Position.X + 800 > posicion.X)
                {
                    Personaje Objetivo = DetectarPlayerMasCerca(Juego.listaDePersonaje);

                    if (Objetivo.posicion.X - posicion.X > 0)
                    {
                        MoverDerecha();
                        if (Objetivo.posicion.Y < posicion.Y - 35 && enumPlayerEstadoY == EnumPlayerY.NORMAL)
                        {
                            Saltar();
                        }
                    }
                    else
                    {
                        MoverIzquierda();
                    }
                }
        }

        public void VerificacionDanioConAtaques(List<Personaje> listaDePersonaje)
        {

            foreach (Personaje i in listaDePersonaje)
            {
                foreach (Ataque a in i.listaDeAtaque)
                {
                    if (a.ColisionConmigo(rectangulo))
                    {
                        Health -= a.danio;
                        a.activo = false;
                    }
                }
            }
        }

        public Personaje DetectarPlayerMasCerca(List<Personaje> listaDePersonajes)  /// en prueba solo funciona para 2 
        {
            Personaje resultado = Juego.takashi; // asignamos el primer personaje

            foreach (Personaje i in listaDePersonajes) 
            {
                if (Math.Abs(i.posicion.X - posicion.X) < Math.Abs(resultado.posicion.X - posicion.X)) 
                {
                    resultado = i;
                }
            }

            return resultado;
        }

        public void ColisionPersonajeConListaDePersonaje(List<Personaje> lista)
        {
            if (Colision(lista.ToList<Objeto>(), enumColision.TODAS))
            {
                Health--;
            }
        }
        public void ColisionaPowerUp()
        {
            foreach (PowerUp i in Juego.MapaActual.listaPowersUpDelMapa)
            {
                if (i.ColisionConmigo(rectangulo))
                {
                    if (i.activado) 
                    {
                        if (i.tipoPower == enumTipoPowerUp.CURACION) 
                        {
                            if (Health + i.vida < MaximaSaludPlayer)
                            {
                                Health += i.vida; //Curar
                                i.activado = false;
                                boolpowerUp = true;
                            }
                            else
                            {
                                Health = MaximaSaludPlayer;
                                i.activado = false;
                                boolpowerUp = true;
                            }
                        }
                        else if (i.tipoPower == enumTipoPowerUp.VelocidadPlus)
                        {
                            //Health += i.vida; //aumentar velocidad

                        }
                        else if (i.tipoPower == enumTipoPowerUp.ActivacionPower)
                        {
                            //Health += i.vida; // activar poder
                        }
                    }
                }
                
            }
        }
     
    }
}
