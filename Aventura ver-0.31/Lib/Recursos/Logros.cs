﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib.Recursos
{
    [Serializable]
    public class Logros
    {
        public string Nombre;
        public bool Activo = false;

        public Logros() { }
        public Logros(string nombre)
        {
            Nombre = nombre;
        }
    }
}
