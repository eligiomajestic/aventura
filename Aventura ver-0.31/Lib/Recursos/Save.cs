﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using Lib.Entidades;
using Microsoft.Xna.Framework;

namespace Lib.Recursos
{
    [Serializable]
    public class Save
    {
        public string Nombre;
        public int MundoActual;
        public int[] vidaPersonajes = new int[4];
        public int[] puntaje = new int[4];
        public int MonedasGeneral = 0;
        public List<Logros> listLogro = new List<Logros>();


        public Save() { }
        public Save(string name = "SAVE") 
        {
            Nombre = name;
        }

        public void Guardar()
        {
            //cargamos el save con datos a guardar
            MundoActual = Juego.mundoActualAprobado;
            MonedasGeneral = Juego.monedasGeneral;
            vidaPersonajes[0] = Juego.takashi.Vidas;
            vidaPersonajes[1] = Juego.takashi2.Vidas;
            vidaPersonajes[2] = Juego.takashi3.Vidas;
            vidaPersonajes[3] = Juego.takashi4.Vidas;

            puntaje[0] = Juego.takashi.puntuacion;
            puntaje[1] = Juego.takashi2.puntuacion;
            puntaje[2] = Juego.takashi3.puntuacion;
            puntaje[3] = Juego.takashi4.puntuacion;

            listLogro = Juego.listLogros;
 
            

            using (FileStream archivo = new FileStream(Nombre, FileMode.Create))
            {
                XmlSerializer serializador = new XmlSerializer(typeof(Save));
                serializador.Serialize(archivo, this);
            }
        }

        public void Cargar()
        {
            using (FileStream archivo = new FileStream(Nombre, FileMode.OpenOrCreate))
            {
                
                XmlSerializer Deserializador = new XmlSerializer(typeof(Save));
                Save temp = (Save)Deserializador.Deserialize(archivo);

                // aqui cargamos el juego
                Juego.mundoActualAprobado = temp.MundoActual;
                Juego.monedasGeneral = temp.MonedasGeneral;
                Juego.takashi.Vidas = temp.vidaPersonajes[0];
                Juego.takashi2.Vidas = temp.vidaPersonajes[1];
                Juego.takashi3.Vidas = temp.vidaPersonajes[2];
                Juego.takashi4.Vidas = temp.vidaPersonajes[3];

                Juego.takashi.puntuacion = temp.puntaje[0];
                Juego.takashi2.puntuacion = temp.puntaje[1];
                Juego.takashi3.puntuacion = temp.puntaje[2];
                Juego.takashi4.puntuacion = temp.puntaje[3];

                Juego.listLogros = temp.listLogro;
            }
        }

        public void BorrarSalvado() 
        {
            //cargamos el save con datos a guardar
            MundoActual = 1;
            MonedasGeneral = 0;
            vidaPersonajes[0] = 3;
            vidaPersonajes[1] = 3;
            vidaPersonajes[2] = 3;
            vidaPersonajes[3] = 3;

            puntaje[0] = 0;
            puntaje[1] = 0;
            puntaje[2] = 0;
            puntaje[3] = 0;

            Juego.listLogros = new List<Logros>();
            Juego.CargarLogros();
            listLogro = Juego.listLogros;



            using (FileStream archivo = new FileStream(Nombre, FileMode.Create))
            {
                XmlSerializer serializador = new XmlSerializer(typeof(Save));
                serializador.Serialize(archivo, this);
            }

            Juego.salvar = false;
            Juego.estadoDelJuego = enumJuego.Salir;
        }
    }
}
