﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MatrixTransformCamera
{
    public class Camera
    {
        public Vector2 Position;
        public float Rotation ;
        public float Zoom ;

        public Camera()
        {
            Position = Vector2.Zero;
            Zoom = 1f;
        }

        public Matrix TransformMatrix
        {
            get
            {
                return Matrix.CreateRotationZ(Rotation) * Matrix.CreateScale(Zoom) * Matrix.CreateTranslation(Position.X, Position.Y, 0);
            }
        }
        public void ZoomCamera(float ZoomAccion)
        {
            Zoom += ZoomAccion;
        }
        public void RotateCamera(float Rotacion)
        {
            Rotation += MathHelper.ToRadians(Rotacion);
        }
        public void TranslateCamera(Vector2 cameraTranslate)
        {
            Position += cameraTranslate;
        }
    }
}
