﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework;

namespace Lib.Recursos
{
    public struct ImagenAnimacion
    {
        public Texture2D imagen;
    }

    public class Animacion
    {
        Int32 imagenActual = 0;

        Boolean repitiendo = false;
        Boolean detenido = false;
        Boolean moviendo = false;

        public Int32 frames = 0;
        Int32 totalframes = 0;

        float tiempoCambio = 0.0f;
        float tiempoTotal = 0.0f;

        Int32 Inicio = 0;
        Int32 Fin = 0;

        List<ImagenAnimacion> listaImagenes = new List<ImagenAnimacion>();
        Vector2 posicion;
        float escala = 1.0f;
        SpriteEffects efectos = SpriteEffects.None;
        private SpriteBatch batch;

        public float Escala
        {
            set
            {
                escala = value;
            }
        }
        Vector2 origen = Vector2.Zero;
        public Vector2 Origen
        {
            set
            {
                origen = value;
            }
        }

        public Animacion(GraphicsDeviceManager graficos, Vector2 _posicion)
        {
            this.posicion = _posicion;
            this.batch = new SpriteBatch(graficos.GraphicsDevice);
        }

        public void adicionarImagen(Texture2D _imagen, Int32 _frames)
        {
            ImagenAnimacion Celda = new ImagenAnimacion();
            Celda.imagen = _imagen;
            totalframes = _frames;
            listaImagenes.Add(Celda);
        }

        public void ponerPosicion(float x, float y)
        {
            posicion.X = x;
            posicion.Y = y;
        }

        public void Detener()
        {
            if (moviendo)
            {
                detenido = true;
                repitiendo = false;
                moviendo = false;
                tiempoTotal = 0.0f;
                tiempoCambio = 0.0f;
            }
        }

        public void irA(Int32 numero)
        {
            if (moviendo)
                return;
            if (numero < 0 || numero >= listaImagenes.Count)
                return;
            imagenActual = numero;
        }

        public void iniciarTodo(float segundos, Boolean repetir)
        {
            if (!moviendo)
            {
                irA(0);
                detenido = false;
                moviendo = true;
                repitiendo = repetir;
                Inicio = 0;
                Fin = listaImagenes.Count - 1;
                tiempoCambio = segundos / (float)listaImagenes.Count;
            }
        }

        public void iniciar(Int32 _inicio, Int32 _fin, float segundos, Boolean repetir)
        {
            if (!moviendo)
            {
                irA(_inicio);
                detenido = false;
                repitiendo = repetir;
                moviendo = true;
                this.Inicio = _inicio;
                this.Fin = _fin;
                float diferencia = (float)_fin - (float)_inicio;
                tiempoCambio = segundos / diferencia;
            }
        }

        public void Dibujar(SpriteBatch batch,float profundidad, Color color)
        {            
            if (listaImagenes.Count == 0 || imagenActual < 0 || listaImagenes.Count < imagenActual)
                return;
           
            if (totalframes > 0)
            {
                Int32 anchoFrame = listaImagenes[imagenActual].imagen.Width / totalframes;
                Rectangle rectImagen = new Rectangle(anchoFrame * frames, 0, anchoFrame, listaImagenes[imagenActual].imagen.Height);
                batch.Draw(listaImagenes[imagenActual].imagen, posicion, rectImagen, color, 0.0f, origen, new Vector2(escala, escala), efectos, profundidad);
            }
            else
            {
                batch.Draw(listaImagenes[imagenActual].imagen, posicion, null, color, 0.0f, origen, new Vector2(escala, escala), efectos, profundidad);
            }

        }

        public void Actualizar(GameTime gameTime, bool mirandoDeracha)
        {
            if (mirandoDeracha)
            {
                efectos = SpriteEffects.None;
            }
            else 
            {
                efectos = SpriteEffects.FlipHorizontally;
            }
            if (!detenido)
            {
                tiempoTotal += (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (tiempoTotal > tiempoCambio)
                {
                    if (totalframes > 0)
                    {
                        frames++;
                        tiempoTotal = 0.0f;
                        if (repitiendo)
                        {
                            if (frames > Fin)
                                frames = Inicio;
                        }
                        if (frames > Fin)
                        {
                            frames = Fin;
                            moviendo = false;
                        }
                    }
                    else
                    {
                        tiempoTotal = 0.0f;
                        imagenActual++;
                        if (repitiendo)
                        {
                            if (imagenActual > Fin)
                                imagenActual = Inicio;
                        }
                        if (imagenActual > Fin)
                        {
                            imagenActual = Fin;
                            moviendo = false;
                        }
                    }
                }
            }
        }
    }
}
