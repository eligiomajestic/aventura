using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using Lib.Entidades;
using MatrixTransformCamera;
using Lib.Recursos;

namespace Lib
{
    public enum enumJuego {Menu, Jugando, Pausa, GameOver, Opciones, Instrucciones, SeleccionMundo, Tienda, Video, Salir, Win }
    public enum enumDificultad { MuyFacil, Facil, Normal, Dificil, MuyDificil}

    public class Juego
    {
        #region Variables del Juego
        public static SpriteFont Texto;
        public static GraphicsDeviceManager graphics;
        public static enumJuego estadoDelJuego;
        public static enumJuego estadoASeguir;
        public static enumDificultad Dificultad;
        public static SpriteBatch spriteBatch;
        public static KeyboardState teclado;
        public static GamePadState control1;
        public static GamePadState control2;
        public static GamePadState control3;
        public static GamePadState control4;
        public static MouseState mouse;
        public static Camera camara;
        public static Song songMenu;
        public static Song songJuego;
        public static Song songBoss;
        public static bool boolEnterPresionado;
        public static bool boolEnterPresionado2;
        public static int cantidadDeJugadores;
        public static bool peleandoConBoss = false;

        //Save
        public static Save save;
        public static bool salvar = true;
        public static int mundoActualAprobado = 1;
        public static int monedasGeneral = 100;
        public static List<Logros> listLogros;

        public static int cantidadDeMundos = 10;
        public static Dictionary<String, String> dict;
        public static Mapa MapaActual;
        public static float Gravedad;
        public float vibrationAmount;

        public static Personaje takashi;
        public static Personaje takashi2;
        public static Personaje takashi3;
        public static Personaje takashi4;
        public static List<Personaje> listaDePersonaje;
        public static List<Personaje> listaPersonajesVivos;
        public static Personaje Oponente;

        //imagenes de ataques
        public static Texture2D AtaqueDobleFuego;
        public static Texture2D AtaqueLanzador;
        public static Texture2D AtaqueBolaDeNieveTextura;
        public static Texture2D AtaqueEscudo;

        //imagenes de personajes
        public static Texture2D PlayerTexturaStandar;
        public static Texture2D PlayerTexturaCaminando;
        public static Texture2D EnemigoNormalTexturaStandar;
        public static Texture2D EnemigoMuerteTextura;
        public static Texture2D PlayerTexturaSaltando;
        public static Texture2D PlayerTexturaMuerte;
        public static Texture2D PlayerTexturaPowerUp;
        public static Texture2D PlayerTexturaAtaqueNormal;
        public static Texture2D topKiritoTexture;

        public static Texture2D bossTexturaStandar;
        public static Texture2D bossTexturaAtaque1;
        public static Texture2D bossTexturaAtaque2;
        public static Texture2D bossTexturaDano;
        public static Texture2D bossTexturaPreparacion;

        //textura del top
        public static Texture2D topPoderesTextura;
        public static Texture2D marcoSeletTextura;

        //NUEVO  - textura para el power up
        public static Texture2D TexturaPowerUpVida;
        public static Texture2D TexturaMoneda;

        //imagenes de mapa
        public static Texture2D fondo1;
        public static Texture2D fondo2;
        public static Texture2D fondo3;
        public static Texture2D fondo4;
        public static Texture2D fondo5;
        public static Texture2D texturaBlock;
        public static Texture2D texturaBlockInicio;
        public static Texture2D texturaBlockFin;
        public static Texture2D texturaNube;
        public static Texture2D texturaBloqueHierba;
        public static Texture2D texturaBloqueNube;
        public static Texture2D texturaBloqueMadera;
        public static Texture2D HealthBar;
        public static Texture2D texturaTiendaFondo;
        public static Texture2D texturaBlock5;

        //Variable que te indica si el nivel esta activo
        public static bool activo = true;

        //Tienda
        public static List<Item> listaDeItems;
        public static bool semaforoTienda = false;

        public static string nombreVideo;
        #endregion

        public Juego() 
        {
            camara = new Camera();
            save = new Save("SAVE");
            Gravedad = 60;
            vibrationAmount = 0.0f;
            cantidadDeJugadores = 1;
            Dificultad = enumDificultad.MuyFacil;

            listaDeItems = new List<Item>();
            listLogros = new List<Logros>();
            estadoDelJuego = enumJuego.Menu;

            boolEnterPresionado = false;
            boolEnterPresionado2 = false;
            
        }

        public static void InicializarMapa(int NumeroDelMapa) 
        {
            MapaActual = new Mapa(NumeroDelMapa,Dificultad);
        }

        public static void InicializarJugadores() 
        {
            takashi = new Personaje(Enumspersonaje.PLAYER, new Vector2(200, 300));
            takashi2 = new Personaje(Enumspersonaje.PLAYER, new Vector2(300, 300), 2);
            takashi3 = new Personaje(Enumspersonaje.PLAYER, new Vector2(400, 300), 3);
            takashi4 = new Personaje(Enumspersonaje.PLAYER, new Vector2(500, 300), 4);
            listaPersonajesVivos = new List<Personaje>();
        }

        public static void CargarTexturasYDemas(ContentManager Content)
        {
            Texto = Content.Load<SpriteFont>(@"Archivos/texto");
            dict = Content.Load<Dictionary<String, String>>("Archivos/Rutas");
            songMenu = Content.Load<Song>("Musica/Sign");
            songJuego = Content.Load<Song>("Musica/Kokuten");
            songBoss = Content.Load<Song>("Musica/Epic");

            #region Cargando Texturas del Mapa
            texturaNube = Content.Load<Texture2D>(dict["Nubes"]);
            texturaBlock = Content.Load<Texture2D>(dict["MiddleTile"]);
            texturaBlockInicio = Content.Load<Texture2D>(dict["BeginTile"]);
            texturaBlockFin = Content.Load<Texture2D>(dict["EndTile"]);
            texturaBloqueNube = Content.Load<Texture2D>(dict["BloqueNube"]);
            texturaBloqueHierba = Content.Load<Texture2D>(dict["BloqueHierba"]);
            texturaBloqueMadera = Content.Load<Texture2D>(dict["BloqueMadera"]);
            texturaBlock5 = Content.Load<Texture2D>(dict["Bloque5"]);
            fondo1 = Content.Load<Texture2D>(dict["fondo"]);
            fondo2 = Content.Load<Texture2D>(dict["fondo2"]);
            fondo3 = Content.Load<Texture2D>(dict["fondo3"]);
            fondo4 = Content.Load<Texture2D>(dict["fondo4"]);
            fondo5 = Content.Load<Texture2D>(dict["fondo5"]);
            TexturaPowerUpVida = Content.Load<Texture2D>(dict["PowerUpVida"]);
            TexturaMoneda = Content.Load<Texture2D>(dict["Moneda"]);
            texturaTiendaFondo = Content.Load<Texture2D>(dict["TiendaFondo"]);
            #endregion

            #region Cargando Ataques
            AtaqueDobleFuego = Content.Load<Texture2D>(dict["AtaqueDobleFuego"]);
            AtaqueBolaDeNieveTextura = Content.Load<Texture2D>(dict["BolaDeNieve"]);
            AtaqueEscudo = Content.Load<Texture2D>(dict["Escudo"]);
            #endregion

            #region Cargando Texturas de Personajes
            PlayerTexturaStandar = Content.Load<Texture2D>(dict["Standar"]);
            PlayerTexturaCaminando = Content.Load<Texture2D>(dict["HeroeCorriendo"]);
            PlayerTexturaSaltando = Content.Load<Texture2D>(dict["HeroeSalto"]);
            PlayerTexturaMuerte = Content.Load<Texture2D>(dict["HeroeMuerte"]);
            EnemigoNormalTexturaStandar = Content.Load<Texture2D>(dict["EnemigoStandar"]);
            EnemigoMuerteTextura = Content.Load<Texture2D>(dict["EnemigoMuerte"]);
            PlayerTexturaPowerUp = Content.Load<Texture2D>(dict["AnimacionPowerUp"]);
            AtaqueLanzador = Content.Load<Texture2D>(dict["AtaqueLanzador"]);
            PlayerTexturaAtaqueNormal = Content.Load<Texture2D>(dict["PrimerAtaque"]);
            topKiritoTexture = Content.Load<Texture2D>(dict["KiritoTop"]);
            HealthBar = Content.Load<Texture2D>(dict["BarraDeSalud"]);

            topPoderesTextura = Content.Load<Texture2D>(dict["TopPoderes"]);
            marcoSeletTextura = Content.Load<Texture2D>(dict["MarcoSelet"]);

            //boss
            bossTexturaStandar = Content.Load<Texture2D>(dict["BossStandar"]);
            bossTexturaAtaque1 = Content.Load<Texture2D>(dict["BossAtaque1"]);
            bossTexturaAtaque2 = Content.Load<Texture2D>(dict["BossAtaque2"]);
            bossTexturaDano = Content.Load<Texture2D>(dict["BossDano"]);
            bossTexturaPreparacion = Content.Load<Texture2D>(dict["BossPreparacion"]);
            #endregion
        }

        public static void ActualizarJuego()
        {
            if (!peleandoConBoss) 
            {
                if (-camara.Position.X > 32 * 190) 
                {
                    peleandoConBoss = true;
                    MediaPlayer.Play(songBoss);
                }
            }
            if (listaPersonajesVivos.Count <= 0)
            {
                Juego.estadoDelJuego = enumJuego.GameOver; // ahora mismo nos manda al gameover
            }

            //limpia los items que no se buelven a usar de las listar
            LimpiadorDeListas();
        }

        public static void ActualizarVariables() 
        {
            teclado = Keyboard.GetState();
            mouse = Mouse.GetState();
            control1 = GamePad.GetState(PlayerIndex.One);
            control2 = GamePad.GetState(PlayerIndex.Two);
            control3 = GamePad.GetState(PlayerIndex.Three);
            control4 = GamePad.GetState(PlayerIndex.Four);

            if(teclado.IsKeyUp(Keys.Enter))
            {
                boolEnterPresionado = false;
                boolEnterPresionado2 = false;
            }

            if (teclado.IsKeyUp(Keys.NumPad3) && teclado.IsKeyUp(Keys.D3) && control1.Buttons.B == ButtonState.Released)
            {
                Juego.takashi.boolCambioArma = true;
            }
            if (teclado.IsKeyUp(Keys.B) && control2.Buttons.B == ButtonState.Released)
            {
                Juego.takashi2.boolCambioArma = true;
            }
            if (teclado.IsKeyUp(Keys.L) && control3.Buttons.B == ButtonState.Released)
            {
                Juego.takashi3.boolCambioArma = true;
            }
            if (teclado.IsKeyUp(Keys.NumPad9) && control4.Buttons.B == ButtonState.Released)
            {
                Juego.takashi4.boolCambioArma = true;
            }
        }

        public static void MoverPersonajeTeclado(Personaje personaje, GameTime gameTime)
        {

            GamePadState temp = new GamePadState();
            bool KeyParaRiba = false;
            bool KeyParaAbajo = false;
            bool KeyParaDerecha = false;
            bool KeyParaIzquierda = false;
            bool KeyA = false;
            bool KeyB = false;
            bool KeyC = false;

            if (personaje.AlivePersonaje)
            {
                if(personaje.numeroDePlayer == 1)
                {
                    #region Personaje/Player 1
                temp = control1;

                if (teclado.IsKeyDown(Keys.Up))
                    KeyParaRiba = true;
                if (teclado.IsKeyDown(Keys.Left))
                    KeyParaIzquierda = true;
                if (teclado.IsKeyDown(Keys.Right))
                    KeyParaDerecha = true;
                if (teclado.IsKeyDown(Keys.Down))
                    KeyParaAbajo = true;
                if (teclado.IsKeyDown(Keys.NumPad1) || teclado.IsKeyDown(Keys.D1))
                    KeyA = true;
                if (teclado.IsKeyDown(Keys.NumPad2) || teclado.IsKeyDown(Keys.D2))
                    KeyB = true;
                if (teclado.IsKeyDown(Keys.NumPad3) || teclado.IsKeyDown(Keys.D3))
                    KeyC = true;

                    personaje.index = PlayerIndex.One;

                #endregion
                }
                else if(personaje.numeroDePlayer == 2)
                {
                    #region Personaje/Player 2
                    temp = control2;

                    if (teclado.IsKeyDown(Keys.W))
                        KeyParaRiba = true;
                    if (teclado.IsKeyDown(Keys.A))
                        KeyParaIzquierda = true;
                    if (teclado.IsKeyDown(Keys.D))
                        KeyParaDerecha = true;
                    if (teclado.IsKeyDown(Keys.S))
                        KeyParaAbajo = true;
                    if (teclado.IsKeyDown(Keys.C))
                        KeyA = true;
                    if (teclado.IsKeyDown(Keys.V))
                        KeyB = true;
                    if (teclado.IsKeyDown(Keys.B))
                        KeyC = true;
                    personaje.index = PlayerIndex.Two;
                    #endregion
                } 
                else  if(personaje.numeroDePlayer == 3)
                {
                    #region Personaje/Player 3
                    temp = control3;

                    if (teclado.IsKeyDown(Keys.T))
                        KeyParaRiba = true;
                    if (teclado.IsKeyDown(Keys.F))
                        KeyParaIzquierda = true;
                    if (teclado.IsKeyDown(Keys.H))
                        KeyParaDerecha = true;
                    if (teclado.IsKeyDown(Keys.G))
                        KeyParaAbajo = true;
                    if (teclado.IsKeyDown(Keys.J))
                        KeyA = true;
                    if (teclado.IsKeyDown(Keys.K))
                        KeyB = true;
                    if (teclado.IsKeyDown(Keys.L))
                        KeyC = true;

                    personaje.index = PlayerIndex .Three;
                    #endregion
                } 
                else  if(personaje.numeroDePlayer == 4)
                {
                    #region Personaje/Player 4
                    temp = control4;

                    if (teclado.IsKeyDown(Keys.Home))
                        KeyParaRiba = true;
                    if (teclado.IsKeyDown(Keys.Delete))
                        KeyParaIzquierda = true;
                    if (teclado.IsKeyDown(Keys.PageDown))
                        KeyParaDerecha = true;
                    if (teclado.IsKeyDown(Keys.End))
                        KeyParaAbajo = true;
                    if (teclado.IsKeyDown(Keys.NumPad7))
                        KeyA = true;
                    if (teclado.IsKeyDown(Keys.NumPad8))
                        KeyB = true;
                    if (teclado.IsKeyDown(Keys.NumPad9))
                        KeyC = true;

                    personaje.index = PlayerIndex.Four;
                    #endregion
                }
            }

            #region ataques y movimiento
            if (!personaje.inmortal)
            {
                //movimiento
                if ((KeyParaRiba && personaje.enumPlayerEstadoY == EnumPlayerY.NORMAL && personaje.boolLanzo) ||
                    (temp.IsConnected && (temp.Buttons.A == ButtonState.Pressed && personaje.enumPlayerEstadoY == EnumPlayerY.NORMAL && personaje.boolLanzo)))
                {
                    personaje.Saltar();
                }
                if ((KeyParaDerecha && personaje.boolLanzo && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL) ||
                    (temp.IsConnected && temp.DPad.Right == ButtonState.Pressed && personaje.boolLanzo) ||
                    temp.ThumbSticks.Left.X > 0.1f && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL)
                {
                    personaje.MoverDerecha();
                }
                else if ((KeyParaIzquierda && personaje.boolLanzo && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL) ||
                    (temp.IsConnected && temp.DPad.Left == ButtonState.Pressed && personaje.boolLanzo && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL) ||
                    temp.ThumbSticks.Left.X < -0.1f && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL)
                {
                    personaje.MoverIzquierda();
                }
                //para atacar
                if ((KeyB && personaje.boolLanzo && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL) || (temp.Buttons.X == ButtonState.Pressed && personaje.boolLanzo && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL))
                {
                    //Se verifica sim se puede lanzar
                    if (Juego.listLogros.Single(s => s.Nombre == personaje.ataqueSelet.ToString()).Activo)
                    {
                        GamePad.SetVibration(personaje.index, 1f, 1f);

                        //lanza ataque
                        if (personaje.ataqueSelet != EnumsAtaque.Escudo)
                        {
                            personaje.Atacar(personaje.ataqueSelet);
                        }
                    }
                }
                if ((KeyA && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL) || (temp.Buttons.A == ButtonState.Pressed && personaje.enumPlayerAtaques != EnumPlayerAtaques.ATAQUENORMAL))
                {
                    GamePad.SetVibration(personaje.index, 1f, 1f);
                    //lanza ataque
                    personaje.Atacar(EnumsAtaque.NORMAL);
                }
            }

            //cambiar de arma
            if (KeyC && personaje.boolCambioArma || (temp.Buttons.B == ButtonState.Pressed && personaje.boolCambioArma))
            {
                personaje.CambiarDeArma();
                personaje.boolCambioArma = false;
            }
            #endregion

            personaje.AnularAceleracion(teclado);
        }

        public static void ActualizarPersonajes(GameTime gameTime) 
        {

            if(cantidadDeJugadores >= 1)
            {
                MoverPersonajeTeclado(takashi,gameTime);
                takashi.Actualizar(gameTime);                
                if(cantidadDeJugadores >= 2)
                {
                    MoverPersonajeTeclado(takashi2,gameTime);
                    takashi2.Actualizar(gameTime);                    
                    if(cantidadDeJugadores >= 3)
                    {
                        MoverPersonajeTeclado(takashi3,gameTime);
                        takashi3.Actualizar(gameTime);
                        if (cantidadDeJugadores >= 4)
                        {
                            MoverPersonajeTeclado(takashi4,gameTime);
                            takashi4.Actualizar(gameTime);
                        }
                    }
                }
            }
        }

        public static void PintarPersonajes()
        {
            //pintamos jugadores
            #region Players
            if (cantidadDeJugadores >= 1)
            {
                takashi.Pintate(spriteBatch);
                if (cantidadDeJugadores >= 2)
                {
                    takashi2.Pintate(spriteBatch);
                    if (cantidadDeJugadores >= 3)
                    {
                        takashi3.Pintate(spriteBatch);
                        if (cantidadDeJugadores >= 4)
                        {
                            takashi4.Pintate(spriteBatch);
                        }
                    }
                }
            }
            #endregion

            //pintammos resto de personajes
            #region Enemigos y NPC
            foreach (Personaje i in MapaActual.listaEnemigos)
            {
                i.Pintate(spriteBatch);
            }

            foreach (Personaje NPC in MapaActual.listaNPC)
            {
                NPC.Pintate(spriteBatch);
            }
            #endregion
        }

        public static void ActualizarCamara(List<Personaje> lista) 
        {

            switch (lista.Count)
            {
                case 1:
                    camara.Position = -new Vector2(lista[0].posicion.X - 400, 0);
                    break;
                case 2:
                    camara.Position = -new Vector2((lista[0].posicion.X + lista[1].posicion.X) / 2 - 350, 0);
                    break;
                case 3:
                    camara.Position = -new Vector2((lista[0].posicion.X + lista[1].posicion.X + lista[2].posicion.X) / 3 - 350, 0);
                    break;
                case 4:
                    camara.Position = -new Vector2((lista[0].posicion.X + lista[1].posicion.X + lista[2].posicion.X + lista[3].posicion.X) / 4 - 350, 0);
                    break;
            }
        }

        public static void InicializarAnimacionesPlayer(Juego juego)
        {
            //Global
            takashi.InicializarAnimaciones();
            takashi2.InicializarAnimaciones();
            takashi3.InicializarAnimaciones();
            takashi4.InicializarAnimaciones();
        }

        public static void CargarAnimacionesDePersonajes() 
        {
            takashi.CargarAnimaciones();
            takashi2.CargarAnimaciones();
            takashi3.CargarAnimaciones();
            takashi4.CargarAnimaciones();
        }

        public static List<Personaje> IntroducirPersonajeEnUnaLista()
        {
            List<Personaje> temp = new List<Personaje>();

            temp.Add(Juego.takashi);
            listaPersonajesVivos.Add(takashi);

            if (Juego.cantidadDeJugadores == 2)
            {
                temp.Add(Juego.takashi2);
                listaPersonajesVivos.Add(takashi2);
            }
            else if (Juego.cantidadDeJugadores == 3)
            {
                temp.Add(Juego.takashi2);
                listaPersonajesVivos.Add(takashi2);
                temp.Add(Juego.takashi3);
                listaPersonajesVivos.Add(takashi3);
            }
            else if (Juego.cantidadDeJugadores == 4)
            {
                temp.Add(Juego.takashi2);
                listaPersonajesVivos.Add(takashi2);
                temp.Add(Juego.takashi3);
                listaPersonajesVivos.Add(takashi3);
                temp.Add(Juego.takashi4);
                listaPersonajesVivos.Add(takashi4);
            }


            return temp;
        }

        public static List<Personaje> PersonajesVivos()
        {
            List<Personaje> listTemporal = new List<Personaje>();

            //Verificar
            if (takashi.AlivePersonaje && cantidadDeJugadores >= 1)
            {
                listTemporal.Add(takashi);
            }
            if (takashi2.AlivePersonaje && cantidadDeJugadores >= 2)
            {
                listTemporal.Add(takashi2);
            }
            if (takashi3.AlivePersonaje && cantidadDeJugadores >= 3)                    
            {                       
                listTemporal.Add(takashi3);
            }                       
            if (takashi4.AlivePersonaje && cantidadDeJugadores >= 4)                        
            {                          
                listTemporal.Add(takashi4);    
            }

            listaPersonajesVivos = listTemporal;
            return listaPersonajesVivos;
        }

        public static void LimpiadorDeListas() 
        {
            foreach(Personaje i in listaDePersonaje)
            {
                #region Lipia Las Listas de los players
                for (int j = 0; j < i.listaDeAtaque.Count; j++)
                {
                    if (!i.listaDeAtaque[j].activo || 
                        i.listaDeAtaque[j].posicion.X > (-Juego.camara.Position.X) + 1200 ||
                        i.listaDeAtaque[j].posicion.X < (-Juego.camara.Position.X) - 400 ||
                        i.listaDeAtaque[j].posicion.Y >  1200 ||
                        i.listaDeAtaque[j].posicion.Y < -1200) 
                    {
                        i.listaDeAtaque.Remove(i.listaDeAtaque[j]);
                        j++;
                    }

                }
                #endregion

                #region Limpia lista de los Enemigos
                foreach (Personaje eTemp in Juego.MapaActual.listaEnemigos) 
                {
                    for (int x = 0; x < eTemp.listaDeAtaque.Count; x++)
                    {
                        if (!eTemp.listaDeAtaque[x].activo ||
                            eTemp.listaDeAtaque[x].posicion.X > (-Juego.camara.Position.X) + 1200 ||
                            eTemp.listaDeAtaque[x].posicion.X < (-Juego.camara.Position.X) - 400 ||
                            eTemp.listaDeAtaque[x].posicion.Y >  1200 ||
                            eTemp.listaDeAtaque[x].posicion.Y < -1200) 
                        {
                            eTemp.listaDeAtaque.Remove(eTemp.listaDeAtaque[x]);
                            x++;
                        }
                    }
                }
                #endregion

                #region Limpia lista de los mapas
                //limpia los powers up
                for (int n = 0; n < Juego.MapaActual.listaPowersUpDelMapa.Count; n++)
                {
                    if (!Juego.MapaActual.listaPowersUpDelMapa[n].activado) 
                        {
                            Juego.MapaActual.listaPowersUpDelMapa.Remove(Juego.MapaActual.listaPowersUpDelMapa[n]);
                            n++;
                        }
                }

                //limpia los enemigos
                for (int y = 0; y < Juego.MapaActual.listaEnemigos.Count; y++)
                {
                    if (!Juego.MapaActual.listaEnemigos[y].AlivePersonaje && Juego.MapaActual.listaEnemigos[y].aniMuertePersonaje.frames == 7)
                    {
                        Juego.MapaActual.listaEnemigos.Remove(Juego.MapaActual.listaEnemigos[y]);
                        y++;
                    }
                }

                #endregion
            }
        }

        public static void CargarLogros()
        {
            Juego.listLogros.Add(new Logros("Final"));
            Juego.listLogros.Add(new Logros("Fuego"));
            Juego.listLogros.Add(new Logros("Nieve"));
            Juego.listLogros.Add(new Logros("Escudo"));
            Juego.listLogros.Add(new Logros("Vida"));
        }

        public static void Salir() 
        {
            //bool salvar a ver si se va a salvar
            if (salvar)
            {
                save.Guardar();
            }
        }

        public static void SumarMonedas()
        {
            monedasGeneral += takashi.monedas + takashi2.monedas + takashi3.monedas + takashi4.monedas;
            takashi.monedas = 0;
            takashi2.monedas =0;
            takashi3.monedas =0;
            takashi4.monedas = 0;
 
        }

        public static void ReproducirVideo(String Nombre, enumJuego EstadoSiguiente)
        {
            Juego.estadoASeguir = EstadoSiguiente;
            Juego.nombreVideo = Nombre;

            Juego.estadoDelJuego = enumJuego.Video;
        }
    }
}

