using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{

    public class VideoComponente : Microsoft.Xna.Framework.DrawableGameComponent
    {
        public Video videoIntro;
        public Video videoFinal;
        public Video Videocomienzo;
        public VideoPlayer reproductorVideo;
        public Texture2D videoTextura;
        public SpriteBatch batch;

        public VideoComponente(Game game)
            : base(game)
        {
            reproductorVideo = new VideoPlayer();
        }

       
        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            videoIntro = Game.Content.Load<Video>("Video/Intro");
            videoFinal = Game.Content.Load<Video>("Video/Final");
            Videocomienzo = Game.Content.Load<Video>("Video/Comienzo");
            base.LoadContent();
        }

        
        public override void Update(GameTime gameTime)
        {
            switch (Juego.nombreVideo)
            {
                case"Intro":
                    reproductorVideo.Play(videoIntro);  
                    break;

                case "Final":
                    reproductorVideo.Play(videoFinal);
                    break;

                case "Comienzo":
                    reproductorVideo.Play(Videocomienzo);
                    break;

                default:
                    break;
            }

            //Condicion para salir cuando el video termina
            if(reproductorVideo.PlayPosition >= reproductorVideo.Video.Duration - TimeSpan.FromSeconds(0.5) || Juego.teclado.IsKeyDown(Keys.Escape))
            {
                Juego.estadoDelJuego = Juego.estadoASeguir;
                reproductorVideo.Stop(); // no funciona
            }
            
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //Solo llama a GetTexture si un video es corriendo o esta pausado
            if (reproductorVideo.State != MediaState.Stopped)
                videoTextura = reproductorVideo.GetTexture();


            //Dibujar hacia el rectangulo va a adecuar el video para que quepa en la ventana
            Rectangle screen = new Rectangle(GraphicsDevice.Viewport.X,
                GraphicsDevice.Viewport.Y,
                GraphicsDevice.Viewport.Width,
                GraphicsDevice.Viewport.Height);

            //Dibuja el video, si tenemos una textura para dibujar
            if (videoTextura != null)
            {
                batch.Begin();
                batch.Draw(videoTextura, screen, Color.White);
                batch.End();
            }
            base.Draw(gameTime);
        }
    }
}
