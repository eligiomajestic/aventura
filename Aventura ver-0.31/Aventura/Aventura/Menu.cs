using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{

    public class Menu : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch batch;

        SpriteFont font;

        Texture2D mainMenu;
        

        public Menu(Game game)
            : base(game)
        {
            
        }
        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            mainMenu = Game.Content.Load<Texture2D>("imagenes/MenuTest");
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            OpcionesMenu();
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            batch.Begin();

            batch.Draw(mainMenu, new Vector2(0,0),null, Color.White,0f,Vector2.Zero,1f,SpriteEffects.None, 1f);
            batch.DrawString(font, "Jugar", new Vector2(335, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Opciones", new Vector2(335, 310), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Instrucciones", new Vector2(335, 370), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Salir", new Vector2(335, 430), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            batch.End();
            base.Draw(gameTime);
        }

        public void OpcionesMenu()
        {
            //Entra a opciones desde el menu principal
            if (Juego.mouse.X > 335 && Juego.mouse.X < 486)
            {
                if (Juego.mouse.Y > 310 && Juego.mouse.Y < 337)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed && Juego.estadoDelJuego == enumJuego.Menu)
                    {
                        Juego.estadoDelJuego = enumJuego.Opciones;
                    }
                }
            }

            //Va a jugando del menu principal
            if (Juego.mouse.X > 330 && Juego.mouse.X < 436)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 290)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed && Juego.estadoDelJuego == enumJuego.Menu)
                    {
                        Juego.ReproducirVideo("Intro", enumJuego.SeleccionMundo);
                    }
                }
            }

            //Entra a instrucciones desde el menu principal
            if (Juego.mouse.X > 335 && Juego.mouse.X < 486)
            {
                if (Juego.mouse.Y > 370 && Juego.mouse.Y < 404)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed && Juego.estadoDelJuego == enumJuego.Menu)
                    {
                        Juego.estadoDelJuego = enumJuego.Instrucciones;
                    }
                }
            }

            //Sale del juego haciendo click en la opci�n Salir.
            if (Juego.mouse.X > 335 && Juego.mouse.X < 486)
            {
                if (Juego.mouse.Y > 430 && Juego.mouse.Y < 461)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Salir();
                        Game.Exit();
                    }
                }
            }
        }
    }
}
