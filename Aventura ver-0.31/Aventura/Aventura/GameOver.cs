using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{
    public class GameOver : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch batch;
        Texture2D gameOver;

        public GameOver(Game game): base(game)
        {
        }
        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            gameOver = Game.Content.Load<Texture2D>("imagenes/GameOverTest");
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Two, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Three, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Four, 0f, 0f);

            if(Juego.teclado.IsKeyDown(Keys.Enter))
            {
                Juego.SumarMonedas();
                Jugando.IrASeleccionDeMundo();
            }

            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            batch.Begin();
            batch.Draw(gameOver, new Vector2(0, 0), Color.White);
            batch.End();
            base.Draw(gameTime);
        }
    }
}
