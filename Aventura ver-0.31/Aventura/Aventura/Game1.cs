using System;
using System.Collections.Generic;
using System.Linq;
using Lib;
using Lib.Entidades;
using Lib.Recursos;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Aventura
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        Juego juego = new Juego();
        GameOver gameover;
        Menu mainMenu;
        Pausa pauseMenu;
        Opciones opcionesMenu;
        Instrucciones instrucMenu;
        Jugando dentroJuego;
        SeleccionarMundo selecMundo;
        Tienda tienda;
        VideoComponente vid;
        Victoria win;
        
        public Game1()
        {
            Juego.graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content"; 
            Juego.graphics.PreferredBackBufferHeight = 600;
            Juego.graphics.PreferredBackBufferWidth = 800;
        }

        protected override void Initialize()
        {

            IsMouseVisible = true;
            gameover = new GameOver(this);
            gameover.Enabled = false;
            gameover.Initialize();

            mainMenu = new Menu(this);
            mainMenu.Enabled = false;
            gameover.Initialize();

            pauseMenu = new Pausa(this);
            pauseMenu.Enabled = false;
            pauseMenu.Initialize();

            opcionesMenu = new Opciones(this);
            opcionesMenu.Enabled = false;
            opcionesMenu.Initialize();

            instrucMenu = new Instrucciones(this);
            instrucMenu.Enabled = false;
            instrucMenu.Initialize();

            dentroJuego = new Jugando(this);
            dentroJuego.Enabled = false;
            dentroJuego.Initialize();

            selecMundo = new SeleccionarMundo(this);
            selecMundo.Enabled = false;
            selecMundo.Initialize();

            tienda = new Tienda(this);
            tienda.Enabled = false;
            tienda.Initialize();

            vid = new VideoComponente(this);
            vid.Enabled = false;
            vid.Initialize();

            win = new Victoria(this);
            win.Enabled = false;
            win.Initialize();

 
            base.Initialize();
        }

        protected override void LoadContent()
        {

            Juego.spriteBatch = new SpriteBatch(GraphicsDevice);

            //inicializamos jugadores luego de cargar todo lo necesario
            Juego.InicializarJugadores();
            Juego.listaDePersonaje = Juego.IntroducirPersonajeEnUnaLista();

            //"animaciones de players"
            Juego.InicializarAnimacionesPlayer(juego);
            Juego.CargarAnimacionesDePersonajes();

            Juego.CargarLogros();

            Juego.save.Cargar();

            Juego.ReproducirVideo("Comienzo", enumJuego.Menu);
        }

        protected override void UnloadContent()
        {   }

        protected override void Update(GameTime gameTime)
        {
            //Actualizamos variables del juego
            Juego.ActualizarVariables();  
       
            //Maneja los componentes conforme a su estado
            ManejadorDeComponentes();


            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }




        //Agrege musica conforme al estado
        public void ManejadorDeComponentes()
        {
            switch (Juego.estadoDelJuego)
            {
                case enumJuego.Jugando:
                    if (!dentroJuego.Enabled)
                    {
                        Components.Add(dentroJuego);
                        dentroJuego.Enabled = true;
                    }
                    if (selecMundo.Enabled)
                    {
                        MediaPlayer.Play(Juego.songJuego);
                        MediaPlayer.IsRepeating = true;
                        Components.Remove(selecMundo);
                        selecMundo.Enabled = false;
                    }
                    if (pauseMenu.Enabled)
                    {
                        MediaPlayer.Resume();
                        Components.Remove(pauseMenu);
                        pauseMenu.Enabled = false;
                    }
                    break;
                case enumJuego.SeleccionMundo:
                    if (!MediaPlayer.IsRepeating)
                    {
                        MediaPlayer.Play(Juego.songMenu);
                        MediaPlayer.IsRepeating = true;
                    }
                    if (!selecMundo.Enabled)
                    {
                        Components.Add(selecMundo);
                        selecMundo.Enabled = true;
                    }
                    if (mainMenu.Enabled)
                    {
                        Components.Remove(mainMenu);
                        mainMenu.Enabled = false;
                    }
                    if (gameover.Enabled)
                    {
                        MediaPlayer.Play(Juego.songMenu);
                        Components.Remove(gameover);
                        gameover.Enabled = false;
                    }
                    if (tienda.Enabled)
                    {
                        Components.Remove(tienda);
                        tienda.Enabled = false;
                    }
                    if (vid.Enabled)
                    {
                        Components.Remove(vid);
                        vid.Enabled = false;
                    }
                    break;
                case enumJuego.Menu:
                    if (!mainMenu.Enabled)
                    {
                        if (!MediaPlayer.IsRepeating)
                        {
                            MediaPlayer.Play(Juego.songMenu);
                            MediaPlayer.IsRepeating = true;
                        }
                        Components.Add(mainMenu);
                        mainMenu.Enabled = true;
                    }
                    if (opcionesMenu.Enabled)
                    {
                        Components.Remove(opcionesMenu);
                        opcionesMenu.Enabled = false;
                    }

                    if (instrucMenu.Enabled)
                    {
                        Components.Remove(instrucMenu);
                        instrucMenu.Enabled = false;
                    }
                    if (vid.Enabled)
                    {
                        Components.Remove(vid);
                        vid.Enabled = false;
                    }
                    break;
                case enumJuego.Opciones:
                    if (mainMenu.Enabled)
                    {
                        Components.Remove(mainMenu);
                        mainMenu.Enabled = false;
                    }
                    if (!opcionesMenu.Enabled)
                    {
                        Components.Add(opcionesMenu);
                        opcionesMenu.Enabled = true;
                        
                    }
                    break;
                case enumJuego.Instrucciones:
                    if (mainMenu.Enabled)
                    {
                        Components.Remove(mainMenu);
                        mainMenu.Enabled = false;
                    }
                    if (!instrucMenu.Enabled)
                    {
                        Components.Add(instrucMenu);
                        instrucMenu.Enabled = true;
                    }
                    
                    break;
                case enumJuego.GameOver:
                    if (!gameover.Enabled)
                    {
                        MediaPlayer.Stop();
                        Components.Add(gameover);
                        gameover.Enabled = true;
                    }
                    break;
                case enumJuego.Pausa:
                    if (!pauseMenu.Enabled)
                    {
                        Components.Add(pauseMenu);
                        pauseMenu.Enabled = true;
                        MediaPlayer.Pause();
                    }

                    break;
                case enumJuego.Tienda:
                    if (!tienda.Enabled)
                    {
                        Components.Add(tienda);
                        tienda.Enabled = true;
                    }
                    if (selecMundo.Enabled)
                    {
                        Components.Remove(selecMundo);
                        selecMundo.Enabled = false;
                    }
                    break;

                case enumJuego.Video:
                    MediaPlayer.Stop();
                    MediaPlayer.IsRepeating = false;
                    if (!vid.Enabled)
                    {
                        Components.Add(vid);
                        vid.Enabled = true;
                    }
                    if (mainMenu.Enabled)
                    {
                        Components.Remove(mainMenu);
                        mainMenu.Enabled = false;
                    }

                    break;

                case enumJuego.Salir:

                    Juego.Salir();
                    Exit();

                    break;

                case enumJuego.Win:
                    MediaPlayer.Stop();
                    MediaPlayer.IsRepeating = false;
                    if (!win.Enabled)
                    {
                        Components.Add(win);
                        win.Enabled = true;
                    }

                    if(dentroJuego.Enabled)
                    {
                        Components.Remove(dentroJuego);
                        dentroJuego.Enabled = false;
                    }

                    break;
            }

       
        }
    }
}
