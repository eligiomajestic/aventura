using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;


namespace Aventura
{

    public class Jugando : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        public Jugando(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            Juego.CargarTexturasYDemas(Game.Content);
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (Juego.estadoDelJuego != enumJuego.Pausa)
            {
                //Actualizamos Personajes
                Juego.ActualizarPersonajes(gameTime);

                //Actualizar lista de personajes vivos
                Juego.PersonajesVivos();

                //Actualizamos camara
                Juego.ActualizarCamara(Juego.PersonajesVivos());

                //Actualizamos mapa
                Juego.MapaActual.Actualizar(gameTime);

                //Actualizamos el estado del juego
                Juego.ActualizarJuego();

                //Entra al estado pausa
                if (Juego.teclado.IsKeyDown(Keys.Enter) && Juego.estadoDelJuego == enumJuego.Jugando && !Juego.boolEnterPresionado2)
                {
                    Juego.estadoDelJuego = enumJuego.Pausa;
                    Juego.boolEnterPresionado = true;
                }

                //verificamos si no hay boss
                if (Juego.peleandoConBoss )
                    VerificarVictoria();

                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            Juego.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Juego.camara.TransformMatrix);

            //pintamos personajes       
            Juego.PintarPersonajes();

            //pintamos el mapa
            Juego.MapaActual.Pintate(Juego.spriteBatch);

            Juego.spriteBatch.End();
            base.Draw(gameTime);
        }

        public void Ganar()
        {
            Juego.SumarMonedas();

            if (Juego.mundoActualAprobado <= Juego.MapaActual.numeroDelMapa)
                Juego.mundoActualAprobado++;

            Juego.save.Guardar();

            //aqui cambiamos de componente al win
            Juego.estadoDelJuego = enumJuego.Win;
        }

        public static void IrASeleccionDeMundo()
        {
            
            Juego.estadoDelJuego = enumJuego.SeleccionMundo;
            Juego.listaDePersonaje = Juego.IntroducirPersonajeEnUnaLista();
            foreach (Personaje i in Juego.listaDePersonaje)
            {
                i.Vidas = 3;
                i.Health = i.MaximaSaludPlayer;
                i.AlivePersonaje = true;
                i.posicion = new Vector2(300, 300);
            }
        }
        public void VerificarVictoria() 
        {
            bool win = true;
            foreach (Personaje i in Juego.MapaActual.listaEnemigos)
            {
                if (i.enumTipo == Enumspersonaje.BOSS)
                {
                    if (i.AlivePersonaje)
                    {
                        win = false;
                        break;
                    }
                }
            }
            if (win)
            {
                Ganar();
            }
        }

    }
}
