using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib.Entidades;
using Lib;
using System.Speech.Recognition;


namespace Aventura
{
    public class Tienda : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        Texture2D flechaIzquierda;
        Texture2D texturaPowerUpEscudo;
        Texture2D texturaPowerUpFuego;
        Texture2D texturaPowerUpNieve;
        Texture2D texturaPowerUpFinal;
        Texture2D texturaVida;
        public SpriteFont font;
        public SpeechRecognitionEngine ReconocedorDeVoz = new SpeechRecognitionEngine();
        bool primeraVez = true;
        bool click = false;
        

        public Tienda(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {

            texturaPowerUpEscudo = Game.Content.Load<Texture2D>("imagenes/PowerUpEscudoTienda");
            texturaPowerUpFuego = Game.Content.Load<Texture2D>("imagenes/PowerUpFuegoTienda");
            texturaPowerUpNieve = Game.Content.Load<Texture2D>("imagenes/PowerUpNieveTienda");
            texturaPowerUpFinal = Game.Content.Load<Texture2D>("imagenes/PowerUpFinalTienda");
            texturaVida = Game.Content.Load<Texture2D>("imagenes/Vida");
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            flechaIzquierda = Game.Content.Load<Texture2D>("imagenes/FlechaIzquierda");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (primeraVez)
            {
                ReconocedorDeVoz.SetInputToDefaultAudioDevice();
                ReconocedorDeVoz.LoadGrammar(new DictationGrammar());
                ReconocedorDeVoz.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(ReconocedorDeVoz_SpeechRecognized);
                ReconocedorDeVoz.RecognizeAsync(RecognizeMode.Multiple);
                primeraVez = false;
            }
            //Actualizar itens
            ActualizarItems();

            if (!Juego.semaforoTienda)
            {
                CargarListaDeItems();
                Juego.semaforoTienda = true;
            }
            if (Juego.mouse.LeftButton == ButtonState.Pressed && !click) 
            {
                VerificarCliks();
                click = true;
            }

            if (Juego.mouse.LeftButton == ButtonState.Released)
                click = false;


            if (Juego.mouse.X > 0 && Juego.mouse.X < 230)
            {
                if (Juego.mouse.Y > 520 && Juego.mouse.Y < 580)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.estadoDelJuego = enumJuego.SeleccionMundo;
                    }
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            batch.Begin();

            batch.Draw(Juego.texturaTiendaFondo, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(flechaIzquierda, new Vector2(30, 490), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "Atras", new Vector2(160, 530), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(Juego.Texto, "Monedas : "+ Juego.monedasGeneral.ToString(), new Vector2(550,550), Color.Yellow, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            PintarItems(batch);
            batch.End();

            base.Draw(gameTime);
        }

        public void CargarListaDeItems()
        {
            if(Juego.listLogros.Single(s => s.Nombre == "Escudo").Activo)
                Juego.listaDeItems.Add(new Item(750, "Escudo", texturaPowerUpEscudo, new Vector2(20, 10), false));
            else
                Juego.listaDeItems.Add(new Item(750, "Escudo", texturaPowerUpEscudo, new Vector2(20, 10), true));

            if (Juego.listLogros.Single(s => s.Nombre == "Fuego").Activo)
                Juego.listaDeItems.Add(new Item(1024, "Fuego", texturaPowerUpFuego, new Vector2(170, 10), false));
            else
                Juego.listaDeItems.Add(new Item(1024, "Fuego", texturaPowerUpFuego, new Vector2(170, 10), true));

            if (Juego.listLogros.Single(s => s.Nombre == "Nieve").Activo)
                Juego.listaDeItems.Add(new Item(300, "Nieve", texturaPowerUpNieve, new Vector2(340, 10), false));
            else
                Juego.listaDeItems.Add(new Item(300, "Nieve", texturaPowerUpNieve, new Vector2(340, 10), true));

            if (Juego.listLogros.Single(s => s.Nombre == "Final").Activo)
                Juego.listaDeItems.Add(new Item(9999, "Final", texturaPowerUpFinal, new Vector2(510, 10), false));
            else
                Juego.listaDeItems.Add(new Item(9999, "Final", texturaPowerUpFinal, new Vector2(510, 10), true));

                Juego.listaDeItems.Add(new Item(50, "Vida", texturaVida, new Vector2(40, 220), true));
        }

        public void PintarItems(SpriteBatch batch)
        {
            foreach (Item i in Juego.listaDeItems)
            {
                i.Pintate(batch);
            }
        }

        public void VerificarCliks() 
        {
            foreach (Item i in Juego.listaDeItems)
            {
                if (i.ColisionConmigo(new Rectangle((int)Juego.mouse.X, (int)Juego.mouse.Y, 3, 3))) 
                {
                    if (Juego.monedasGeneral >= i.precio && i.disponible) 
                    {
                        i.Comprar();
                    }

                }
            }
        }

        public void ActualizarItems() 
        {
            foreach (Item i in Juego.listaDeItems)    
            {
                i.Actualizar();
            }
        }

        void ReconocedorDeVoz_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            foreach (RecognizedWordUnit i in e.Result.Words)
            {
                switch (i.Text)
                {
                    case"only":
                        Juego.monedasGeneral += 1;
                        break;

                    case "yes":
                        Juego.monedasGeneral += 2;
                        break;

                    case "no":
                        Juego.monedasGeneral -= 5;
                        break;

                    default:
                        break;
                }
            }
        }
    }
    
}
