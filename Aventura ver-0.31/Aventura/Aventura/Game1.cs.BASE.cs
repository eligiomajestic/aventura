using System;
using System.Collections.Generic;
using System.Linq;
using Lib;
using Lib.Entidades;
using Lib.Recursos;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Aventura
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        Juego juego = new Juego();
        GameOver gameover;
        Menu mainMenu;
        Pausa pauseMenu;
        
        public Game1()
        {
            Juego.graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content"; 
            Juego.graphics.PreferredBackBufferHeight = 600;
        }

        protected override void Initialize()
        {

            IsMouseVisible = true;
            gameover = new GameOver(this);
            gameover.Enabled = false;
            gameover.Initialize();

            mainMenu = new Menu(this);
            mainMenu.Enabled = false;
            gameover.Initialize();

            pauseMenu = new Pausa(this);
            pauseMenu.Enabled = false;
            pauseMenu.Initialize();
 
            base.Initialize();
        }

        protected override void LoadContent()
        {
            juego.spriteBatch = new SpriteBatch(GraphicsDevice);
            Juego.Texto = Content.Load<SpriteFont>(@"Archivos/texto");
            juego.dict = Content.Load<Dictionary<String, String>>("Archivos/Rutas");
            juego.songMenu = Content.Load<Song>("Musica/Sign");
            juego.songJuego = Content.Load<Song>("Musica/Kokuten");

            #region Cargando Texturas del Mapa
            Juego.texturaNube = Content.Load<Texture2D>(juego.dict["Nubes"]);
            Juego.texturaBlock = Content.Load<Texture2D>(juego.dict["MiddleTile"]);
            Juego.texturaBlockInicio = Content.Load<Texture2D>(juego.dict["BeginTile"]);
            Juego.texturaBlockFin = Content.Load<Texture2D>(juego.dict["EndTile"]);
            Juego.fondo1 = Content.Load<Texture2D>(juego.dict["fondo"]);
            Juego.TexturaPowerUpVida = Content.Load<Texture2D>(juego.dict["PowerUpVida"]);
            Juego.TexturaMoneda = Content.Load<Texture2D>(juego.dict["Moneda"]);
            

            #endregion

            #region Cargando Ataques
            Juego.AtaqueDobleFuego = Content.Load<Texture2D>(juego.dict["AtaqueDobleFuego"]);
            
            #endregion

            #region Cargando Texturas de Personajes
            Juego.PlayerTexturaStandar = Content.Load<Texture2D>(juego.dict["Standar"]);
            Juego.PlayerTexturaCaminando = Content.Load<Texture2D>(juego.dict["HeroeCorriendo"]);
            Juego.PlayerTexturaSaltando = Content.Load<Texture2D>(juego.dict["HeroeSalto"]);
            Juego.PlayerTexturaMuerte = Content.Load<Texture2D>(juego.dict["HeroeMuerte"]);
            Juego.EnemigoNormalTexturaStandar = Content.Load<Texture2D>(juego.dict["EnemigoStandar"]);
            Juego.PlayerTexturaPowerUp = Content.Load<Texture2D>(juego.dict["AnimacionPowerUp"]);
            Juego.AtaqueLanzador = Content.Load<Texture2D>(juego.dict["AtaqueLanzador"]);
            Juego.topKiritoTexture = Content.Load<Texture2D>(juego.dict["KiritoTop"]);
            Juego.HealthBar = Content.Load<Texture2D>(juego.dict["BarraDeSalud"]);
            #endregion

            //inicializamos el mapa
            juego.InicializarMapa();

            //inicializamos jugadores luego de cargar todo lo necesario
            juego.InicializarJugadores();
            Juego.listaDePersonaje = juego.IntroducirPersonajeEnUnaLista();

            //"animaciones de players"
            juego.InicializarAnimacionesPlayer(juego);
            juego.CargarAnimacionesDePersonajes();

            //ahora llenamos la listas del mapa luego de tener la texturas
            Juego.MapaActual.LlenarListas();

            //inicializamos los enemigos y npc luego de cargar el mapa
            juego.InicializarAnimacionesDeEnemigosYNPC();

            //cargamos las animaciones de npc y enemigos
            juego.CargarAnimacionesDeEnemigosYNPC();

        }

        protected override void UnloadContent()
        {   }

        protected override void Update(GameTime gameTime)
        {
            //actualizamos variables del juego
            juego.ActualizarVariables();
            if (!Juego.cualquierMenuActivo)
            {
                //actualizamos Juego
                juego.ActualizarJuego();

                //actualizamos Personajes
                juego.ActualizarPersonajes(gameTime);

                //actualizar lista de personajes vivos
                //juego.PersonajesVivos();

                //actualizamos camara
                juego.ActualizarCamara(juego.PersonajesVivos());

                //actulizamos mapa
                Juego.MapaActual.Actualizar(gameTime);
            }
            ManejadorDeComponentes();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            //Esto se le agrega un foreach para los niveles de juego
            if (Juego.activo)
            {
                juego.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Juego.camara.TransformMatrix);

                //pintamos personajes
                juego.PintarPersonajes();

                //pintamos el mapa
                Juego.MapaActual.Pintate(juego.spriteBatch);

                juego.spriteBatch.End();
                base.Draw(gameTime);
            }
            
        }

        //Agrege musica conforme al estado
        public void ManejadorDeComponentes()
        {
            //si el estado del juego es de game over y el jugador no tiene vidas
            //presenta la pantalla de game over y abilita su componente
            if (Juego.estadoDelJuego == enumJuego.GameOver && !gameover.Enabled)
            {
                if (Juego.cantidadDeJugadores == 1 && Juego.takashi.Vidas <= 0
                    || Juego.cantidadDeJugadores == 2 && Juego.takashi.Vidas <= 0 && Juego.takashi2.Vidas <= 0
                    || Juego.cantidadDeJugadores == 3 && Juego.takashi.Vidas <= 0 && Juego.takashi2.Vidas <= 0 && Juego.takashi3.Vidas <= 0
                    || Juego.cantidadDeJugadores == 4 && Juego.takashi.Vidas <= 0 && Juego.takashi2.Vidas <= 0 && Juego.takashi3.Vidas <= 0 && Juego.takashi4.Vidas <=0)
                {
                    MediaPlayer.Stop();
                    Components.Add(gameover);
                    gameover.Enabled = true;
                    Juego.cualquierMenuActivo = false;
                }
            }

            //abilitamos el menu principal si el estado del juego es del menu
            if (Juego.estadoDelJuego == enumJuego.Menu && !mainMenu.Enabled)
            {
                Components.Add(mainMenu);
                mainMenu.Enabled = true;
                MediaPlayer.Play(juego.songMenu);
                MediaPlayer.IsRepeating = true;
            }

            //quitamos el menu principal y cambiamos el estado de juego a jugando
            if (juego.mouse.X > 335 && juego.mouse.X < 486 )
            {
                if (juego.mouse.Y > 250 && juego.mouse.Y < 310 )
                {
                    if (juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        MediaPlayer.Play(juego.songJuego);
                        Components.Remove(mainMenu);
                        mainMenu.Enabled = false;
                        Juego.estadoDelJuego = enumJuego.Jugando;
                        Juego.cualquierMenuActivo = false;
                        
                    }
                }
            }

            //pausamos el juego
            if (juego.teclado.IsKeyDown(Keys.Enter) && !Juego.cualquierMenuActivo && !gameover.Enabled && !Juego.boolEnterPresionado2)
            {
                Components.Add(pauseMenu);
                pauseMenu.Enabled = true;
                Juego.estadoDelJuego = enumJuego.Pausa;
                Juego.cualquierMenuActivo = true;
                Juego.boolEnterPresionado = true;
                MediaPlayer.Pause();
            }

            //reanudamos el juego
           if (juego.teclado.IsKeyDown (Keys.Enter) && !Juego.boolEnterPresionado)
           {
               MediaPlayer.Resume();
               Components.Remove(pauseMenu);
               pauseMenu.Enabled = false;
               Juego.estadoDelJuego = enumJuego.Jugando;
               Juego.cualquierMenuActivo = false;
               Juego.boolEnterPresionado2 = true;
           }
        }
    }
}
