using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{

    public class Pausa : Microsoft.Xna.Framework.DrawableGameComponent
    {

        SpriteBatch batch;
        SpriteFont font;
        Texture2D pauseMenuFondo;

        

        public Pausa(Game game)
            : base(game)
        {
        }
        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            pauseMenuFondo = Game.Content.Load<Texture2D>("imagenes/FondoPausa");
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            //Vuelve a jugando del estado pausa
            if (Juego.teclado.IsKeyDown(Keys.Enter) && !Juego.boolEnterPresionado && Juego.estadoDelJuego == enumJuego.Pausa)
            {
                Juego.estadoDelJuego = enumJuego.Jugando;
                Juego.boolEnterPresionado2 = true;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {

            batch.Begin();
            batch.Draw(pauseMenuFondo, new Vector2(290,200), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            
            batch.DrawString(font, "Resume: Enter", new Vector2(300, 300), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            batch.End();
            base.Draw(gameTime);
        }
    }
}
