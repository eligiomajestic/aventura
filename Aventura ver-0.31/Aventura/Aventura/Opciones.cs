using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{
    public class Opciones : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        SpriteFont font;

        Texture2D seleccion;
        Texture2D fondo;
        Texture2D flechaIzquierda;
        Texture2D barraLaiga;

        public Opciones(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {

            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            seleccion = Game.Content.Load<Texture2D>("imagenes/SelectOpcion");
            fondo = Game.Content.Load<Texture2D>("imagenes/MenuTest");
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            flechaIzquierda = Game.Content.Load<Texture2D>("imagenes/FlechaIzquierda");
            barraLaiga = Game.Content.Load<Texture2D>("imagenes/barraLaiga");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            BorraLoSalvado();
            VuelveAlMenuPrincipal();
            CambiarDificultad();
            CantidadJugadores();
            
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            batch.Begin();
            batch.Draw(fondo, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(barraLaiga, new Vector2(250, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "Borrar Salvado", new Vector2(280, 10), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "MuyFacil", new Vector2(30, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Facil", new Vector2(180, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Normal", new Vector2(330, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Dificil", new Vector2(480, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "MuyDificil", new Vector2(620, 250), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            batch.DrawString(font, "1 Player", new Vector2(30, 370), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "2 Player", new Vector2(180, 370), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "3 Player", new Vector2(330, 370), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "4 Player", new Vector2(480, 370), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(flechaIzquierda, new Vector2(30, 490), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "Atras", new Vector2(160, 530), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            

            batch.Draw(seleccion, new Vector2((((int)(Juego.Dificultad) * 150)+20), 240), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.Draw(seleccion, new Vector2((((int)(Juego.cantidadDeJugadores-1) * 150)+20), 360), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.End();
            base.Draw(gameTime);
        }

        public void VuelveAlMenuPrincipal()
        {
            if (Juego.mouse.X > 0 && Juego.mouse.X < 230)
            {
                if (Juego.mouse.Y > 520 && Juego.mouse.Y < 580)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed && (Juego.estadoDelJuego == enumJuego.Opciones || Juego.estadoDelJuego == enumJuego.Instrucciones))
                    {
                        Juego.estadoDelJuego = enumJuego.Menu;
                    }
                }
            }
        }

        public void BorraLoSalvado()
        {
            if (Juego.mouse.X > 250 && Juego.mouse.X < 540)
            {
                if (Juego.mouse.Y > 10 && Juego.mouse.Y < 50)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.save.BorrarSalvado();
                    }

                }
            }
        }
        public void CantidadJugadores()
        {
            
            if (Juego.mouse.X > 33 && Juego.mouse.X < 180)
            {
                if (Juego.mouse.Y > 370 && Juego.mouse.Y < 490)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.cantidadDeJugadores = 1;
                    }
                }
            }

            if (Juego.mouse.X > 184 && Juego.mouse.X < 330)
            {
                if (Juego.mouse.Y > 370 && Juego.mouse.Y < 490)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.cantidadDeJugadores = 2;
                    }
                }
            }
            if (Juego.mouse.X > 335 && Juego.mouse.X < 480)
            {
                if (Juego.mouse.Y > 370 && Juego.mouse.Y < 480)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.cantidadDeJugadores = 3;
                    }
                }
            }
            if (Juego.mouse.X > 486 && Juego.mouse.X < 600)
            {
                if (Juego.mouse.Y > 370 && Juego.mouse.Y < 490)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.cantidadDeJugadores = 4;
                    }
                }
            }

        }
        public void CambiarDificultad()
        {
            if (Juego.mouse.X > 20 && Juego.mouse.X < 150)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 360)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Dificultad = enumDificultad.MuyFacil;
                    }
                }
            }

            if (Juego.mouse.X > 180 && Juego.mouse.X < 300)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 360)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Dificultad = enumDificultad.Facil;
                    }
                }
            }

            if (Juego.mouse.X > 330 && Juego.mouse.X < 450)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 360)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Dificultad = enumDificultad.Normal;
                    }
                }
            }
            if (Juego.mouse.X > 480 && Juego.mouse.X < 600)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 360)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Dificultad = enumDificultad.Dificil;
                    }
                }
            }
            if (Juego.mouse.X > 630 && Juego.mouse.X < 750)
            {
                if (Juego.mouse.Y > 250 && Juego.mouse.Y < 360)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Dificultad = enumDificultad.MuyDificil;
                    }
                }
            }

        }
    }
}
