using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{
    public class Instrucciones : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        SpriteFont font;
        SpriteFont font1;

        Texture2D flechas1;
        Texture2D flechas2;
        Texture2D flechas3;
        Texture2D flechas4;
        Texture2D ataque1;
        Texture2D ataque2;
        Texture2D ataque3;
        Texture2D ataque4;

        Texture2D BolaNieve;
        Texture2D DobleFuego;
        Texture2D Escudo;

        Texture2D fondo;
        Texture2D flechaIzquierda;
        public Instrucciones(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            font1 = Game.Content.Load<SpriteFont>("Archivos/texto");
            fondo = Game.Content.Load<Texture2D>("imagenes/MenuTest");
            flechas1 = Game.Content.Load<Texture2D>("imagenes/Player1Arrows");
            flechas2 = Game.Content.Load<Texture2D>("imagenes/Player2Arrows");
            flechas3 = Game.Content.Load<Texture2D>("imagenes/Player3Arrows");
            flechas4 = Game.Content.Load<Texture2D>("imagenes/Player4Arrows");
            ataque1 = Game.Content.Load<Texture2D>("imagenes/Player1Buttons");
            ataque2 = Game.Content.Load<Texture2D>("imagenes/Player2Buttons");
            ataque3 = Game.Content.Load<Texture2D>("imagenes/Player3Buttons");
            ataque4 = Game.Content.Load<Texture2D>("imagenes/Player4Buttons");

            DobleFuego = Game.Content.Load<Texture2D>("imagenes/InstruccionDobleFuego");
            BolaNieve = Game.Content.Load<Texture2D>("imagenes/InstruccionBolaNieve");
            Escudo = Game.Content.Load<Texture2D>("imagenes/InstruccionesEscudo");
            flechaIzquierda = Game.Content.Load<Texture2D>("imagenes/FlechaIzquierda");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            VuelveAlMenuPrincipal();
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            batch.Begin();
            batch.Draw(fondo, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "Dirrecionales: Izquierda, Arriba y Derecha", new Vector2(60, 150), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "P1", new Vector2(105, 180), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(flechas1, new Vector2(60, 200), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P2", new Vector2(225, 180), Color.Yellow, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(flechas2, new Vector2(190, 210), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P3", new Vector2(355, 180), Color.LightGreen, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(flechas3, new Vector2(320, 210), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P4", new Vector2(485, 180), Color.LightCoral, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(flechas4, new Vector2(450, 210), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);


            batch.DrawString(font, "Ataque: Espada , Ataque , Cambiar Ataque", new Vector2(60, 320), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "P1", new Vector2(105, 360), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(ataque1, new Vector2(60, 400), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P2", new Vector2(225, 360), Color.Yellow, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(ataque2, new Vector2(190, 400), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P3", new Vector2(355, 360), Color.LightGreen, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(ataque3, new Vector2(320, 400), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "P4", new Vector2(485, 360), Color.LightCoral, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(ataque4, new Vector2(450, 400), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            batch.DrawString(font1, "Ataque: Bola De Nieve", new Vector2(250, 0), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font1, "Special: Congelacion", new Vector2(250, 20), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(BolaNieve, new Vector2(300, 70), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            batch.DrawString(font1, "Ataque: Doble Fuego", new Vector2(550, 0), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font1, "Special: Ninguno", new Vector2(550, 20), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(DobleFuego, new Vector2(600, 70), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            batch.DrawString(font1, "Defensa: Escudo", new Vector2(30, 0), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font1, "Special: Ninguno", new Vector2(30, 20), Color.LightSkyBlue, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.Draw(Escudo, new Vector2(80, 70), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            batch.Draw(flechaIzquierda, new Vector2(30, 490), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            batch.DrawString(font, "Atras", new Vector2(160, 530), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.End();
            base.Draw(gameTime);
        }

        public void VuelveAlMenuPrincipal()
        {
            if (Juego.mouse.X > 0 && Juego.mouse.X < 230)
            {
                if (Juego.mouse.Y > 520 && Juego.mouse.Y < 580)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed && (Juego.estadoDelJuego == enumJuego.Opciones || Juego.estadoDelJuego == enumJuego.Instrucciones))
                    {
                        Juego.estadoDelJuego = enumJuego.Menu;
                    }
                }
            }
        }
    }
}
