using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;
using Lib.Entidades;


namespace Aventura
{

    public enum eMundoSeleccionado {Primero = 1 ,Segundo, Tercero, Cuarto, Quinto, Sexto}

    public class SeleccionarMundo : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteFont font2;
        eMundoSeleccionado mundoSelect = eMundoSeleccionado.Primero;


        public static Texture2D texturaHipnosis;
        public static Texture2D texturaMarcoCirculo;
        public Texture2D texturaMundo1;
        public Texture2D texturaMundo2;
        public Texture2D texturaMundo3;
        public Texture2D texturaMundo4;
        public Texture2D texturaMundo5;

        public Texture2D texturaFondoMapa;
        public Texture2D texturaBarra;
        public Texture2D texturaBarraGrande;
        public Texture2D Info1;
        public Texture2D Info2;
        public Texture2D Info3;
        public Texture2D Info4;
        public Texture2D Info5;
        public Texture2D Info6;
        public static Vector2 posicion;
        public bool Semaforo = false;
        float cambiaColor = 0.2f;
        float TiempoActual;

        Color Colorante2 =  Color.White;
        Color Colorante3 = Color.White;
        Color Colorante4 = Color.White;
        Color Colorante5 = Color.White;
        public SeleccionarMundo(Game game)
            : base(game)
        {
        }

        public override void Initialize()
        {
            LoadContent();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            texturaHipnosis = Game.Content.Load<Texture2D>("ImagenesMapa/hipnosis");
            texturaMarcoCirculo = Game.Content.Load<Texture2D>("imagenes/marcoMundoCircular");
            texturaMundo1 = Game.Content.Load<Texture2D>("ImagenesMapa/Mundo1");
            texturaMundo2 = Game.Content.Load<Texture2D>("ImagenesMapa/Mundo2");
            texturaMundo3 = Game.Content.Load<Texture2D>("ImagenesMapa/Mundo3");
            texturaMundo4 = Game.Content.Load<Texture2D>("ImagenesMapa/Mundo4");
            texturaMundo5 = Game.Content.Load<Texture2D>("ImagenesMapa/Mundo5");

            texturaFondoMapa = Game.Content.Load<Texture2D>("ImagenesMapa/BGMapa");
            texturaBarra = Game.Content.Load<Texture2D>("imagenes/Barra");
            texturaBarraGrande = Game.Content.Load<Texture2D>("imagenes/BarraGrande");
            Info1 = Game.Content.Load<Texture2D>("ImagenesMapa/info1");
            Info2 = Game.Content.Load<Texture2D>("ImagenesMapa/info2");
            Info3 = Game.Content.Load<Texture2D>("ImagenesMapa/info3");
            Info4 = Game.Content.Load<Texture2D>("ImagenesMapa/info4");
            Info5 = Game.Content.Load<Texture2D>("ImagenesMapa/info5");
            Info6 = Game.Content.Load<Texture2D>("ImagenesMapa/info6");
            font2 = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            base.LoadContent();
        }
 

        public override void Update(GameTime gameTime)
        {
            if (Juego.mundoActualAprobado < 2) 
            {
                Colorante2 = Color.DarkRed;
                Colorante3 = Color.DarkRed;
                Colorante4 = Color.DarkRed;
                Colorante5 = Color.DarkRed;
            }
            else if (Juego.mundoActualAprobado < 3)
            {
                Colorante2 = Color.White;
                Colorante3 = Color.DarkRed;
                Colorante4 = Color.DarkRed;
                Colorante5 = Color.DarkRed;
            }else if (Juego.mundoActualAprobado < 4)
            {
                Colorante2 = Color.White;
                Colorante3 = Color.White;
                Colorante4 = Color.DarkRed;
                Colorante5 = Color.DarkRed;
            }
            else if (Juego.mundoActualAprobado < 5)
            {
                Colorante2 = Color.White;
                Colorante3 = Color.White;
                Colorante4 = Color.White;
                Colorante5 = Color.DarkRed;
            }
            else 
            {
                Colorante2 = Color.White;
                Colorante3 = Color.White;
                Colorante4 = Color.White;
                Colorante5 = Color.White;
            }

            if (Juego.teclado.IsKeyUp(Keys.Right) && Juego.teclado.IsKeyUp(Keys.Left))
            {
                Semaforo = false;
            }

            if (Juego.mundoActualAprobado == 1)
            { 


            }
            ManejadorDeTeclado();

            OpcionesSeleccionMundo();

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            Juego.spriteBatch.Begin();
            //20x 35y (tama�o textura de mundos = 117x 92y)
            TiempoActual += gameTime.ElapsedGameTime.Milliseconds;
            if (TiempoActual > 100)
            {
                if (cambiaColor < 1f)
                {
                    cambiaColor += 0.5f;
                }
                else if (cambiaColor >= 0.2f)
                    cambiaColor -= 0.5f;

                TiempoActual = 0;
            }

            Juego.spriteBatch.Draw(texturaFondoMapa, new Vector2(0, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            Juego.spriteBatch.Draw(texturaHipnosis, new Vector2(100, 0), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);

            //Pintar previw de los mundos
            Juego.spriteBatch.Draw(texturaMundo1, new Vector2(410, 15), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            Juego.spriteBatch.Draw(texturaMundo2, new Vector2(620, 205), null, Colorante2, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            Juego.spriteBatch.Draw(texturaMundo3, new Vector2(510, 465), null, Colorante3, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            Juego.spriteBatch.Draw(texturaMundo4, new Vector2(165, 445), null, Colorante4, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            Juego.spriteBatch.Draw(texturaMundo5, new Vector2(120, 155), null, Colorante5, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);

            Juego.spriteBatch.Draw(texturaMarcoCirculo, new Vector2(posicion.X, posicion.Y), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 1f);
            //luna
            Juego.spriteBatch.Draw(texturaBarra, new Vector2(-100, -100), null, Color.White, 0f, Vector2.Zero, 8f, SpriteEffects.None, 0f);

            Juego.spriteBatch.Draw(texturaBarra, new Vector2(25, 440), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            Juego.spriteBatch.Draw(texturaBarra, new Vector2(615, 440), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            Juego.spriteBatch.DrawString(font2, "Tienda", new Vector2(50, 500), Color.Red* cambiaColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            Juego.spriteBatch.DrawString(font2, "Salir", new Vector2(650, 500), Color.Red* cambiaColor, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            PintarInfo(Juego.spriteBatch);

            Juego.spriteBatch.End();
            base.Draw(gameTime);
        }

        public void OpcionesSeleccionMundo()
        {
            
            if (Juego.teclado.IsKeyDown(Keys.A))
            {
                if ((int)mundoSelect == 6)
                {
                    Juego.ReproducirVideo("Final", enumJuego.Menu);
                }
                else
                {
                    if ((int)mundoSelect <= Juego.mundoActualAprobado)
                    {
                        EntrarAlJuego((int)mundoSelect);
                    }
                }
            }

            if (Juego.mouse.X > 49 && Juego.mouse.X < 152) //101
            {
                if (Juego.mouse.Y > 506 && Juego.mouse.Y < 526) // 475
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                        Juego.estadoDelJuego = enumJuego.Tienda;
                }
            }

            if (Juego.mouse.X > 649 && Juego.mouse.X < 729)
            {
                if (Juego.mouse.Y > 506 && Juego.mouse.Y < 526)
                {
                    if (Juego.mouse.LeftButton == ButtonState.Pressed)
                    {
                        Juego.Salir();
                        Game.Exit();
                    }
                }
            }
        }
        public void ManejadorDeTeclado() 
        {
            if (!Semaforo && Juego.teclado.IsKeyDown(Keys.Right))
            {
                mundoSelect += 1;
                Semaforo = true;
            }
            if (!Semaforo && Juego.teclado.IsKeyDown(Keys.Left))
            {
                mundoSelect -= 1;
                Semaforo = true;
            }

            if ((int)mundoSelect > Juego.cantidadDeMundos+1)
                mundoSelect = eMundoSeleccionado.Primero;
            if ((int)mundoSelect <= 0)
                mundoSelect = eMundoSeleccionado.Quinto;

            if (!Juego.listLogros.Single(s => s.Nombre == "Final").Activo && (int)mundoSelect == 6)
            {
                mundoSelect = eMundoSeleccionado.Primero;
            }
            PosicionDelMArco();
        }

        public void EntrarAlJuego(int mapa)
        {
            Juego.MapaActual = new Mapa(mapa, Juego.Dificultad);
            Juego.estadoDelJuego = enumJuego.Jugando;
        }

        public void PosicionDelMArco() 
        {
            if ((int)mundoSelect == 1)
            {
                posicion.X = 390;
                posicion.Y = -20;
            }
            else if ((int)mundoSelect == 2)
            {
                posicion.X = 600;
                posicion.Y = 170;
            }
            else if ((int)mundoSelect == 3)
            {
                posicion.X = 490;
                posicion.Y = 430;
            }
            else if ((int)mundoSelect == 4)
            {
                posicion.X = 145;
                posicion.Y = 410;
            }
            else if ((int)mundoSelect == 5)
            {
                posicion.X = 100;
                posicion.Y = 120;
            }
            else if ((int)mundoSelect == 6)
            {
                posicion.X = 325;
                posicion.Y = 210;
            }
        }
        public void PintarInfo(SpriteBatch batch)
        {
            switch (mundoSelect)
            {
                case eMundoSeleccionado.Primero:
                    batch.Draw(Info1, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                case eMundoSeleccionado.Segundo:
                    batch.Draw(Info2, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                case eMundoSeleccionado.Tercero:
                    batch.Draw(Info3, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                case eMundoSeleccionado.Cuarto:
                    batch.Draw(Info4, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                case eMundoSeleccionado.Quinto:
                    batch.Draw(Info5, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                case eMundoSeleccionado.Sexto:
                    batch.Draw(Info6, new Vector2(5, 5), null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
                    break;
                default:
                    break;
            }
        }


    }
}
