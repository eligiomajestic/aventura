using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Lib;


namespace Aventura
{

    public class Victoria : Microsoft.Xna.Framework.DrawableGameComponent
    {
        SpriteBatch batch;
        SpriteFont font;
        Texture2D winBg;

        public Victoria(Game game)
            : base(game)
        {

        }

        public override void Initialize()
        {

            batch = new SpriteBatch(Game.GraphicsDevice);
            LoadContent();
            base.Initialize();
        }
        protected override void LoadContent()
        {
            winBg = Game.Content.Load<Texture2D>("imagenes/TrollWin");
            font = Game.Content.Load<SpriteFont>("Archivos/textoMenu");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Two, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Three, 0f, 0f);
            GamePad.SetVibration(PlayerIndex.Four, 0f, 0f);

            if (Juego.teclado.IsKeyDown(Keys.Enter))
            {
                Jugando.IrASeleccionDeMundo();
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            batch.Begin();
            batch.Draw(winBg, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.DrawString(font, "Level Complete", new Vector2(300, 10), Color.Yellow, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
            batch.End();
            base.Draw(gameTime);
        }
    }
}
